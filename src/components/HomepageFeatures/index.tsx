import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';
import Translate from '@docusaurus/Translate';

type FeatureItem = {
  title: string;
  description: string;
};

const FeatureList: FeatureItem[] = [
  {
    title: 'titles.first.title',
    description: 'titles.first.description',
  },
  {
    title: 'titles.second.title',
    description: 'titles.second.description',
  },
  {
    title: 'titles.third.title',
    description: 'titles.third.description',
  }
];

function Feature({ title, description }: FeatureItem) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <img alt='Moby' src='/img/moby.png' />
      </div>
      <div className="text--center padding-horiz--md">
        <h3><Translate id={title} /></h3>
        <p><Translate id={description} /></p>
      </div>
    </div>
  );
}

export default function HomepageFeatures(): JSX.Element {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
