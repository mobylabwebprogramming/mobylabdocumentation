---
title: Category theory
sidebar_position: 1
---

## Contributor
[Silviu-George Pantelimon](https://teams.microsoft.com/l/chat/0/0?users=silviu.pantelimon@upb.ro)

# Disclaimer

This section is still a work in progress.

# Introduction

**Category theory** is a mathematical theory originally founded by Saunders Mac Lane (4 August 1909 – 14 April 2005) and Samuel Eilenberg (30 September 1913 – 30 January 1998) and was a major contribution to algebraic topology. Although most programmers or computer science people have not heard of category theory, it has influenced computer science through various concepts.

An area heavily influenced by category theory is the **functional paradigm**. The concept of **functor**, for example, is taken from category theory and is an indispensable tool for modern programming. The most trivial examples of functors are lists and optionals (maybe type), which in high-level programming languages are ubiquitous. In many cases, programmers also use other mathematical structures, such as **monads**, even if they don't know they are called that. A non-trivial example is the concept of tasks/promises in languages that support asynchronous programming.

Although category theory can be a valuable body of knowledge for a programmer, these materials are more dedicated to promoting that knowledge because they are interesting.

# Useful resources

For most of the material described here, the primary source will be [ncatab](https://ncatlab.org/nlab/show/HomePage). ncatlab is a mathematical encyclopedia focused on category theory, topology and other mathematical theories and we recommend it for more information. Here we will take only what is necessary for the topics discussed, in a condensed form.

We also recommend the recorded courses of [Dr. Bartosz Milewski](https://www.youtube.com/@DrBartosz) and [his blog](https://bartoszmilewski.com/). These are probably the best starting point for those interested in category theory.

<iframe width="560" height="315" src="https://www.youtube.com/embed/I8LbkfSSR58?si=jvUCbHf2pGGFfCAn" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerPolicy="strict-origin-when-cross-origin" allowFullScreen></iframe>

