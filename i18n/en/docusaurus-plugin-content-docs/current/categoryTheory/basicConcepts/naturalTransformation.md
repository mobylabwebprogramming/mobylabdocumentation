---
title: Natural transformations
sidebar_position: 3
---

In the previous section, we discussed functors, but even more important than functors in category theory are **natural transformations**. Informally, a natural transformation is a mapping between functors; through a natural transformation, one functor is transformed into another.

## Definition: Natural transformation

Given two categories $D$, $C$ and two functors $F$, $G : C \rightarrow D$, a natural transformation $\alpha : F \implies G$ is a collection of morphisms such that for $\forall x \in Ob(C)$, $\exists \alpha_{x} = F(x) \rightarrow G(x) \in Hom(D)$, called the component of $\alpha$ at $x$, such that for any morphism $f : x \rightarrow y \in Hom(C)$, the following diagram **commutes**:

<img alt="nat" src="/img/category-theory/natural-transformation.png" width="200" />

When we say that this diagram commutes, we mean that the pairs of morphisms compose diagonally into the same morphism, thus satisfying the following relation:

$$
\alpha_y \circ F(f) = G(f) \circ \alpha_x
$$

# Composition

Natural transformations can also be composed just like morphisms and functors. Given the functors $F$, $G$, $H : C \rightarrow D$ and the natural transformations $\alpha : F \implies G$, $\beta : G \implies H$, there will always exist a natural transformation $\beta \circ \alpha : F \implies H$ with components $\forall x \in Ob(C)$, $\beta_x \circ \alpha_x \in Hom(D)$.

# Connection with programming

Natural transformations are often used in programming, although they are not known by this name. The most common natural transformation used in programming is the **map/fmap/select** function of generic types, which imposes a natural transformation from the identity functor to the application of a generic type that implements this function. For example, we can see how this applies to the generic class for lists in the following diagram:

<img alt="nat" src="/img/category-theory/natural-transformation-ex.png" width="400" />

The vertical morphisms, which are components of the natural transformation, represent the application of the generic type over the initial types, and the **map** function is the one that ensures the existence of horizontal morphisms so that the diagram commutes.

# Resources

* [ncatlab](https://ncatlab.org/nlab/show/natural+transformation)