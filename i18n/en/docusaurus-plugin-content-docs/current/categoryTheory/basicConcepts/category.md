---
title: Category
sidebar_position: 1
---

# Terms

To define what a category is, we need to clarify a few terms. In many cases, we will use the term **set**, a collection of unique elements, and **class** for collections of sets. The use of the term class may depend on the foundational context, such as Zermelo–Fraenkel set theory or von Neumann–Bernays–Gödel set theory. For simplicity, we will use the term set if its elements have no additional properties and the term class if the elements represent structures with properties. Thus, we can define what a category is.

## Definition: Category

A category $C$ is composed of:

- a set $Ob(C)$ whose elements are called **objects**
- a set $Hom(C)$ whose elements are called **arrows** or **morphisms**
- for each morphism $f \in Hom(C)$ there is a pair of objects $s(f), t(f) \in Ob(C)$ called the **domain** and **codomain** of $f$
- for every pair of morphisms $f,g \in Hom(C)$ where $t(f) = s(g)$ there is always a morphism $g \circ f \in Hom(C)$ (read $g$ after $f$) called the **compound**
- for each object $x \in Ob(C)$ there is always a morphism $id_x$
- so that the following properties are satisfied:
- source and destination respect the composition: $s(g \circ f) = s(f)$ and $t(g \circ f) = t(g)$
- source and destination respect the identity: $s(id_{x}) = x$ and $t(id_{x}) = x$
- the composition is associative: $(h \circ g) \circ f = h \circ (g \circ f)$
- the composition satisfies left and right unity: $id_{t(f)} \circ f = f \circ id_{s(f)} = f$

An example of a category might be the following:

```mermaid
flowchart TD
    subgraph Category
        a((a)) -->|id<sub>a</sub>| a((a))
        a((a)) -->|f| b((b))
        b((b)) -->|id<sub>b</sub>| b((b))
        a((a)) -->|g∘f| c((c))
        b((b)) -->|g| c((c))
        c((c)) -->|id<sub>c</sub>| c((c))
    end
```

In principle, a category is a quiver, a directed graph with any number of edges between any two nodes. Here, two properties of the category are important: the fact that the composition of the morphisms is associative and preserves the identity. These properties will be used in the next concept, which is more related to functional programming, namely the **functor**.

# Resources

* Alexander Grothendieck, Section 4 of: Techniques de construction et théorèmes d’existence en géométrie algébrique III: préschémas quotients, Séminaire Bourbaki: années 1960/61, exposés 205-222, Séminaire Bourbaki, no. 6 (1961), Exposé no. 212, [link](http://www.numdam.org/item/?id=SB_1960-1961__6__99_0)
* [ncatlab](https://ncatlab.org/nlab/show/category)