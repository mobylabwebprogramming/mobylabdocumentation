---
title: Basic Concepts in Web Application Development
sidebar_position: 1
---

# Basic Concepts in Web Application Development

In this lab, we will split the web application into two distinct components: **frontend** and **backend**. The frontend represents the visible part of the application for the user and runs in their browser. With the frontend as an independent entity, it will be developed as a **SPA (Single-Page Application)**. Unlike an **MPA (Multi-Page Application)**, an SPA consists of a single page that dynamically changes its content based on user actions and data provided by the backend.

The backend represents an **HTTP server** that exposes data and services through an API, and can also serve static content and web pages. In an MPA application, each page served to the browser is generated through an **HTML template** and populated directly with data by the server, following the **MVC (Model-View-Controller)** programming model. However, in this lab, we will opt for an SPA for the frontend, so the server will expose only data and services through an **API**. For simplicity, the API will be of the **REST (REpresentational State Transfer)** type, where data exchanged between the server and client is in JSON format.

Backend applications can also be divided into two categories: **monolithic** and **microservices-based**. A monolithic backend consists of a single process that includes an HTTP server and encompasses the entire logic of the application. In contrast, microservices represent multiple processes that collaborate to implement the application's logic, with each microservice having its own distinct responsibility in the system. There are advantages and disadvantages to both approaches; in this lab, we will use a monolithic architecture to facilitate the implementation and understanding of the concepts.

:::danger
The lab will guide you in creating a backend with a monolithic architecture, where the entire backend logic is integrated into a single process. The same development principles and concepts apply to a microservices-based architecture. However, microservices architecture is more advanced, requiring a longer period to fully understand, and is not recommended for initial projects, especially if the development team is small.
:::
