---
title: Project Setup
sidebar_position: 2
---

To assist you in implementing the lab project, you can use the .NET project skeleton available on [GitLab](https://gitlab.com/mobylabwebprogramming/dotnetbackend). However, to start, we will create a backend project in the preferred IDE, selecting a template for .NET Core Web API, as shown below.

## Visual Studio

<img alt="img" src="/img/web-programming/firstProjectSelectVisualStudio.png" />
<img alt="img" src="/img/web-programming/projectNamingVisualStudio.png" />
<img alt="img" src="/img/web-programming/projectConfigurationVisualStudio.png" />

## Rider
<img alt="img" src="/img/web-programming/firstProjectRider.png" />

Note that the entry point of the program is the Program.cs file. If "Do not use top-level statements" is not selected, there will be no class with a static main function, but it will exist implicitly. Here, an object representing the web application is created and can be configured, as exemplified in the configuration for authorization, controller mapping, or Swagger configuration. These aspects will be detailed further in future labs.

<img alt="img" src="/img/web-programming/projectViewMainVisualStudio.png" />

In C#, a project is created within a solution. The solution manages multiple projects and can be used to maintain references between them. For example, you can reuse common classes between different backend projects through a shared library referenced by both.

You will also find an appsettings.json file, which contains JSON for application configuration. This will be used to set variables used by the backend. Additionally, you will find a very simple controller that will respond to HTTP requests from clients. It is important to note that Dependency Injection is frequently used in web application development. Although this concept will be detailed further, you can observe in the code that components or classes in the project, such as WeatherForecastController, are not instantiated explicitly. Instantiation is done implicitly by the framework, calculating the component's dependencies and injecting them into the component upon instantiation through the constructor, as with the logger in the given example.

Run the backend application, and a page with Swagger, or its alternative name, OpenAPI Specification, will open in the browser. Swagger is a simple interface for testing HTTP requests without needing an HTTP client. It also describes the entire HTTP API of the server, including the routes and data types exchanged with the client. Try to execute a request to the backend directly from the Swagger page. This feature is important because it facilitates backend testing and describes how the API can be used, allowing for the automatic generation of HTTP clients for applications consuming the exposed API.

<img alt="img" src="/img/web-programming/swagger.png" />

To create a backend, it is important to accumulate the necessary knowledge. These will be gradually exposed in the following sections.

## Lab Tasks

Install the necessary tools for the lab and ask for help if you encounter difficulties.

Follow the code in `WeatherForecastController` and try to modify it using your intuition and testing with Swagger. If you have questions, ask the assistant. More information will be provided in the upcoming labs.

Download the project template from [our group's GitLab](https://gitlab.com/mobylabwebprogramming/dotnetbackend) and try to go through the code, using the explanations in the comments. It may not be necessary for this lab, but you can use it as a starting point for the lab project.

:::tip
If you are interested in .NET capabilities, visit [NuGet](https://www.nuget.org/) and explore the packages for C#. NuGet is a package manager that offers a wide range of libraries that can be installed in projects. For Visual Studio, go to `Tools > NuGet Package Manager > Manage NuGet Package for Solution...`, and for Rider, go to `Tools > NuGet > Manage NuGet Package for Solution`.
:::
