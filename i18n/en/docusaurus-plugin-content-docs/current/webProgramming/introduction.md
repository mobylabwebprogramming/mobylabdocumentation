---
title: Web Programming
sidebar_position: 1
---

<img alt="img" src="/img/web-programming/webProgramming.webp" width="800" style={{margin: "auto", display: "block"}} />

## About

The web programming subject focuses on understanding and developing the skills needed to create and manage web applications and websites. This course combines programming knowledge with concepts specific to the online environment to provide students with the tools and skills needed to create effective and interactive web applications.

Within this discipline, we will use, both in the laboratory and in the development of the project, the following technologies:

* Docker for the desktop
* PostgreSQL as database
* .NET 8 for server development
* React.js for UI implementation

## The purpose of the laboratory

The Web Programming Lab aims to provide students with fundamental knowledge in this field and has been designed to provide them with essential concepts in developing web applications. The first half of the lab will be dedicated to backend development within the .NET platform and the second half will focus on frontend development.

## Course

* [Ciprian Dobre](https://teams.microsoft.com/l/chat/0/0?users=ciprian.dobre@upb.ro)

## The team

* [Bogdan-Gabriel Georgescu](https://teams.microsoft.com/l/chat/0/0?users=bgeorgescu@upb.ro)
* [Silviu-George Pantelimon](https://teams.microsoft.com/l/chat/0/0?users=silviu.pantelimon@upb.ro)

## Project requirements

To make the project you need to choose a project theme for a web application. At the laboratory, .NET (backend) and React (frontend) will be used, but other technologies can also be used, more precisely Java Spring (backend) and Angular (frontend) for those who are familiar with these technologies. Projects written with other technologies such as Node.Js (backend), Django, PHP or Android will not be accepted.

:::tip
While you have the option of going with Java Spring and Angular, we recommend working with lab technologies and starting with our [demo](https://gitlab.com/mobylabwebprogramming) application. You will learn new technologies and at the same time you will be able to implement the application for the project easier and faster because you already have many components that you can use.
:::

:::tip
If you choose to use Spring, see that you can also use it with Kotlin instead of Java. Kotlin is a more flexible language than Java and is interoperable with it, all Maven or Gradle packages can be used with Kotlin.
:::

:::danger
If you work in Spring, please don't use EntityManager or other primitives exposed by the ORM, use the JPA repository interfaces and corresponding annotations to interact with the ORM, and especially use JPQL to make complex SQL queries, don't build by hand SQL requests. Also, for the database use Liquibase or Flyway for migrations if you work with Spring, and for Swagger use the OpenAPI package to have it integrated in the backend.
:::

### Backend

* **Repository Pattern/ORM**
  * Minimum 5 entities (excluding merge tables for many-to-many relationships) – **3 points**
  * Entity relationships (one-to-one, one-to-many, many-to-many) – **3 points**
  * Setting up relationships using FluentAPI/Attributes – **3 points**
* **Basic JWT Auth**
  * Role-based permissions with logic to check rights in the backend – **3 points**
  * Controller authorization – **3 points**
* **CRUD Controllers**
  * Basic controllers (Get with ID, Get with list, Post, Put, Delete + others) – **4 points**
  * Error Handling (error codes) – **3 points**
* **DTOs**
  * Use of DTOs for interaction with controllers (request/response) – **5 points**
* **Services**
    * The implementation of a notification sent by mail for a chosen action (use MailTrap) - **3 points**

### Front end
* **Register**
* Registration form - name, other relevant information, username/email + password/password validation (then the login must work) – **4 points**
* **Routing**
* NavBar - minimum 3 pages (excluding those in the code skeleton) – **4 points**
* **Table views**
* Two pages of paginated tables or lists, with a minimum of 4 columns/4 fields of information displayed each – **4 points**
* Search – **3 points**
* Pagination – **2 points**
* **Add/Edit/Delete data (for each table/list)**
* Add Entry – **2 Points**
* Edit entry – **2 points**
* Delete entry – **2 points**
* Modal/confirmation dialog (on deletion) – **2 points**
* **Feedback form**
* A user feedback form (linked to the backend) with at least a select, a radio button, a checkbox and a text box - **5 points**
* **Bonus**
* Full project presentation by week 9 – **5 points**
* Additional features - **5 points**

:::note
Awarding the bonus is at the assistant's discretion. The bonus was awarded for functionalities that exceed the project's requirements. Ex: Containerization, Figma/XD Mockups, Hosting, CI/CD, Push Notifications, Unit Testing, etc. Also, the bonus for early presentation scales with the score obtained, for example, if the score obtained for the project is 30/60, the bonus awarded is 2.5 points.
:::