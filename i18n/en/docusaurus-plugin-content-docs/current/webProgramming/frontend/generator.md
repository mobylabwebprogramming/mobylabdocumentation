---
title: OpenAPI Generator
sidebar_position: 8
---

## What is OpenAPI?

**OpenAPI Specifications** or **OpenAPI**, formerly known as **Swagger**, is a set of open source specifications and tools that enable developers to define, document, and consume RESTful web services.

:::note
OpenAPI defines a standardized form of API interface description, including information about API routes, parameters, data types, and responses that the service returns.
:::

By using OpenAPI, developers can create clear and easy-to-understand documentation for their web services, making it easier for users of these services to use and integrate them into their own applications. OpenAPI also allows developers to automatically generate client code for interacting with an API, which can reduce the time and effort required to create the API client.

OpenAPI is used by many large companies, including Amazon, Google, and Microsoft, and is supported by the open source community.

## What is OpenAPI Generator?

OpenAPI Generator is an open source tool that allows the automatic generation of client and server code for interacting with a web API, based on an OpenAPI specification. It can be used with a variety of programming languages ​​and platforms, including Java, JavaScript, Python, Ruby, C#, and more.

OpenAPI Generator can be used to generate API client code, which can be included in your React.js application. This allows you to interact with the API using the methods and data types defined in the OpenAPI specification without the need to write client code manually.

In addition, OpenAPI Generator can also be used to generate API server code, which can be used to create your own API based on the OpenAPI specification. This can be useful if you want to provide a custom API for your own applications or to allow other developers to use your web services through an API.

OpenAPI Generator is available as a Java library and can be installed and used as a command line tool. There are also integrations available for different development platforms such as Maven, Gradle, Node.js and others.

::: warning
To be able to use OpenAPI Generator you must have Java installed on your computer.
:::

## Use OpenAPI Generator

To use OpenAPI Generator in a React project, follow these steps:

1. Install OpenAPI Generator using a package manager like npm or yarn. For example, to install OpenAPI Generator using npm, run the following command:

```sh
npm install @openapitools/openapi-generator-cli -g
```

2. Generate client code from the OpenAPI specification using the OpenAPI Generator. For example, to generate client code from a swagger.yaml file and save it in a directory named client, run the following command:

```sh
openapi-generator generate -i swagger.yaml -g javascript -o client
```

Alternatively, for the lab skeleton, start the BE and use the command:

```sh
openapi-generator-cli generate -i http://localhost:5000/swagger/v1/swagger.json -g typescript-fetch -o ./src/infrastructure/apis/client --additional-properties=supportsES6=true
```

3. Inside React components, import the generated functions and use them appropriately.

As an example of use, if the generator generated for you from the OpenApi specifications an API to retrieve users **UserApi**, then you can use the generated API in the following way:

```typescript
import { Configuration, UserApi, ApiUserGetPageGetRequest } from "./apis/client";

const getAuthenticationConfiguration = (token: string | null) => new Configuration(!isNull(token) ? { apiKey: `Bearer ${token}` } : {});
const getToken = () => localStorage.getItem("token") ?? null;
export const getUsers = (page: ApiUserGetPageGetRequest) => new UserApi(getAuthenticationConfiguration(getToken())).apiUserGetPageGet(page);
```