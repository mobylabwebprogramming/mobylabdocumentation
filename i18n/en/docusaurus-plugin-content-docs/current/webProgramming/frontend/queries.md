---
title: ReactQuery
sidebar_position: 7
---

## What is ReactQuery?

ReactQuery is a library for React that provides a simple way to manage state and asynchronous data in a React application. This library uses a concept called "queries" to make calls to the server and handle the returned data. ReactQuery uses local caching to reduce the number of requests to the server and provide a faster user experience.

React Query also offers a number of useful features, such as stale-while-revalidate (SWR), which allows cached data to be displayed immediately, even before it is updated with data from the server, and performance focus , which allows data loading to be deferred until that component is loaded.

:::note
Essentially, React Query makes it easier for developers to manage data and state in React apps, allowing them to focus more on building the interface and less on managing data.
:::

## Using React Query

1. To start using ReactQuery, you need to install it first. You can do this using the npm install command:

```sh
npm install @tanstack/react-query
```

2. After installing React Query, you need to import and configure it in your application. React.js. Typically, this is done in the App.ts file. Here is a simple example:

```tsx showLineNumbers
import React from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';

const queryClient = new QueryClient();

export const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <div className="App">
        {/* Restul componentelor tale */}
      </div>
    </QueryClientProvider>
  );
}
```

3. After you have configured React Query, you can start using the **useQuery** and **useMutation** hook functions to get and update the data. Here is an example of using useQuery:

```tsx showLineNumbers
import React from 'react';
import { useQuery } from "@tanstack/react-query";

type TaskData = { id: number, title: string }

export const TaskList = () => {
  const { data, isLoading, error } = useQuery({ // through this hook function we have already abstracted the state of the request without defining it manually
    queryKey: ["todos"], // the list of keys is used to index the result into a cache and invalidate it after timeout or actions
    queryFn: async () => {
      const response = await fetch('https://jsonplaceholder.typicode.com/todos');
      return await response.json() as TaskData[]; // extract the answer as before
    }
  });

  // depending on the current state we display the component accordingly
  if (error) {
    return <p>{error.message}</p>;
  }

  if (isLoading) {
    return <p>Loading...</p>;
  }

  return <div>
    <h1>Task list:</h1>
    <ul>
      {data?.map(task => <li key={task.id}>{task.title}</li>)} 
    </ul>
  </div>
}
```

:::note
In this example, we used **useQuery** to get a list of to-dos from an external API. useQuery gets an identifier for the query ('todos' in this case) and a query function that will be called to get the data. If the query is loaded (isLoading), a "Loading..." message will be displayed, if an error occurs (error), an error message will be displayed, otherwise the data will be displayed. Notice also how the code is simplified compared to the previously presented version using the ReactQuery library.
:::

To better understand how you can use ReactQuery in your projects we suggest you consult the [official documentation](https://tanstack.com/query/latest/docs/framework/react/overview).