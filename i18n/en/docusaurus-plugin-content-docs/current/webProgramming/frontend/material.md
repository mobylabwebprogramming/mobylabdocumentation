---
title: Material UI
sidebar_position: 5
---

**Material UI** is a component library for React that provides a collection of pre-styled UI components to help create modern and attractive user interfaces. These components are based on Google's Material Design styles and design principles, which were developed to provide a consistent and familiar look and feel to web and mobile applications.

Material UI offers a variety of components, including buttons, checkboxes, text fields, tabs, navigation bars, sidebars, modal windows, charts, and more. These components are easy to use and offer a variety of customization options to match your desired design and look.

Material UI also offers a variety of predefined themes and customization options that allow users to customize the appearance of the app according to their needs and preferences. For example, you can change accent colors, fonts, sizes, and more.

In addition, Material UI is well documented and offers an active community of users who contribute to its development and improvement. It is also compatible with many other popular libraries and frameworks, such as Redux, Next.js, TypeScript, and more. To use Material UI in a React project, you must first install the library using a package manager such as npm or yarn. After that, you can import the desired components into React components and use them as you wish.

For example, to use a Material UI button, you can import the **Button** component from the Material UI library and use it in your React component as follows:

```tsx showLineNumber
import { Button } from '@material-ui/core';

function MyButton() {
  return <Button variant="contained" color="primary">
      Click me
    </Button>
}
```

:::note
Here, the Button component is used to create a button with a Material UI layout that has a **"Click me"** text and a primary color background. Using the customization options provided by Material UI, you can change the accent color, dimensions, and more to match your app's design.
:::

We recommend taking a look at the official documentation from [Material UI](https://mui.com/material-ui/getting-started/), there you can find a lot of examples for any type of component from buttons and lists to to forms and dialogs. The easiest way to learn how to use this library is by working and using the necessary components when you need to use them.