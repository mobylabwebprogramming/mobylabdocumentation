---
title: Lab 1 - Development Environment
sidebar_position: 2
---

The goal of this lab is to lay the foundation for software development by presenting various tools that will assist us in this endeavor. In this lab, we will use the **C#** language to introduce concepts from software development practiced in the industry.

Before starting development, we need to set up the development environment. We need:

- A **version control system**, we will use [git](https://git-scm.com/) along with [Gitlab](https://gitlab.com/)/[Github](https://github.com/). Version control systems have several advantages; first, by using a version control system to manage the code, we can have multiple versions of the source code for the same program, tailored for different instances, and the developer can choose which version to work on. Second, a cloud-hosted version control system like Gitlab facilitates collaboration between team members by sharing the code and its changes. Third, all code changes are recorded in a history with information about who made a modification and when.
- An **IDE (Integrated Development Environment)**, which in our case will be **[Rider](https://www.jetbrains.com/rider/)** for **.NET**. Even though creating an application only requires a text editor and a compiler to turn the source code into an executable, an IDE accelerates software development by offering features like autocompletion, debugging, and static analysis, thereby ensuring higher code quality.
- A programming language, and for this lab, it will be **C#**. The **C#** language is syntactically similar to Java, running on the **.NET** framework in a virtualized environment, much like the **JVM**. An introduction to **C#** can be found [here](/docs/dotnet/introduction/).
- A package manager, for **.NET** this is called **[nuget](https://www.nuget.org/)**.

## Git Version Control System

To use git, you can install it from the [site](https://git-scm.com/) or install it for Linux and Mac using the following command:

```sh showLineNumbers
sudo apt-get install git # Linux
brew install git # Mac
```

Initializing a **git repository** where we will save the code can be done in two ways. The first is from the command line and then pushing the code to a git server:

```sh showLineNumbers
git init
git remote add origin https://gitlab.com/my_project_group/my_project # "origin" is the name of the remote repository used for commands followed by the project URL
```

The second is by creating the repository on the server and then downloading it locally using the clone command:

```sh showLineNumbers
git clone https://gitlab.com/my_project_group/my_project
cd my_project
```

When working with a git repository, we usually create a **branch** related to a task and switch to it, where we can work and submit **commits**, which represent incremental changes over the previous version of the current changes. A **branch** represents a fork in the code history, allowing multiple users to work on the code from a common point.

```sh showLineNumbers
git branch staging # "staging" is the name of the newly created branch, there needs to be at least one commit before
git checkout staging # we move onto the "staing" branch
git checkout -b staging # the branch "staging" is directly created and we obe onto it
git checkout -b staing origin/staging # we move onto the "staging" pange that is found remotely on the server named "origin"
```

To save changes as commits, we need to add the modified files to the commit and then perform the commit.

```sh showLineNumbers
git add . # we add all the changes to the next commit
git add my_file_or_folder # we add all the changes in a file or folder for the next commit
git reset HEAD my_file_or_folder # we take out the changes from the commit in a file or folder
git commit -m "This is a relevant message" # we create a commit on the current branch
```

Once committed, the changes only exist locally. To send them to the server, we need to execute a **push** operation.

```sh showLineNumbers
git push # the commits are send from the current branch to the branch on the default remote server
git push origin # the new commits from the current branch are sent to the "origin" remote
git push origin main # the new commits from the main"" branch are sent to the "origin" remote
git push -all # all branches are sent
```

Other users can download new commits and branches either by cloning the project if they don't have it or through **pull** or **fetch** operations.

```sh showLineNumbers
git pull # the current branch is downloaded
git pull origin # the current branch is downloaded from the "origin" remote
git fetch # all new branches and commits are downloaded from the server
git fetch origin # all new branches and commits are downloaded from the "origin" remote
```

The last operation we need to discuss is the **merge** operation. While the **branch** operation creates a fork in the git history, **merge** unifies two branches, combining the code changes.

:::danger
Be careful when performing **merge**, as it is possible that the same files were modified on different branches, leading to conflicts. These are usually resolved manually using a local utility such as **Git GUI**. Therefore, it's best for those working on the same code to focus on tasks that involve changes to different files to avoid such situations.
:::

:::tip
Typically, when working in production, there are multiple branches with different roles. A good approach is to have two branches, one for **production** and one for **development**. When developers start working on a task, they create a new branch from the **development** branch and apply the code changes there. After the task is completed, a **merge request** is made on the git server via the web interface, where colleagues can review the changes, a process called **review**, and the changes are merged into the development branch once approved. When the team decides to release a new version for production, the **development** branch is merged into the **production** branch after the code has been tested.
:::

We should also mention that you can have files in your project folder that you don't want to share, such as binary files generated after compilation. For this reason, the git system can ignore such files or folders using a **.gitignore** file inside the project. Each line contains the relative path to a file or folder that should be omitted from commits. You can see that these files are ignored by using the **status** command, which is also useful for checking other information about the version control system's state.

```sh showLineNumbers
git status
```

## First C# Project

Using git, we can easily share software projects. However, to have code, we will use a .NET project to see how we can manage it.

Once the IDE and framework dependencies are installed, you can create a new project from the interface. For this lab, we will use a **console app** in **.NET 8**. You will notice the basic structure of a C# project, which is quite similar to that of a Java or C/C++ program.

```csharp showLineNumbers
using System; // Folosind cuvantul cheie using se pot importa clase/simboluri din namespace-ul cerut.

namespace MyNewProject; // Se declara namespace-ul curent in care se afla clasele declarate in fisier.

public class Program // Trebuie sa existe neaparat o clasa pentru a avea fuctia principala.
{
    /* Se declara functia statica Main ca punct de intrare a programului, parametri care pot fi omisi si 
     * reprezinta argumentele in linie de comanda a programului (nu este continut numele executabilului ca in C)
     */
	public static void Main(string[] args) {
	    // ....
    }
}
```

With the project created, you can start the exercises below.

# Exercises

1. Install [Rider](https://www.jetbrains.com/rider/) using your [Jetbrains academic account](https://www.jetbrains.com/community/education/#students) with your institutional email and the [Visual Studio installer](https://visualstudio.microsoft.com/downloads/).
2. Create a new project on [Gitlab](https://gitlab.com/) or [Github](https://github.com/) and clone it locally.
3. Create a console application project in the cloned folder using Rider.
4. Run the project, which prints "Hello world!".
5. Install the F23.StringSimilarity package using [nuget](https://www.nuget.org/).
6. Display the calculated similarity value between the strings "Hello world!" and "Hello class!"
7. Commit the project and push it to a new branch on the server.

# Resources

* [Command List](https://www.geeksforgeeks.org/git-cheat-sheet/)
* [Git Command Learning Website](https://learngitbranching.js.org/)