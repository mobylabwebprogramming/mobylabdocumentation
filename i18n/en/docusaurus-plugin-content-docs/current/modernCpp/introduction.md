---
title: C++ Modern
sidebar_position: 1
---

## Contributor
[Silviu-George Pantelimon](https://teams.microsoft.com/l/chat/0/0?users=silviu.pantelimon@upb.ro)

# Disclaimer

This section is still a work in progress.

# Introduction

This is an introduction to interesting aspects of modern C++. For some time there has been the question of whether C++ is a current language or not. Many of the problems of the C++ language are related to insecurity when it comes to working with memory directly, which can lead to errors and waste of computational resources or the complexity of the language.

But the truth is that these problems are more human in nature, due to the fact that programmers do not know how to work with the language properly and understand it. Also, some problems can be related to the fact that the language is quite old, having been launched in 1985. But old does not necessarily mean outdated. The language has constantly evolved and the standard has been extended to keep up with the current needs of programmers. Programmers now have several tools at their disposal to use the language to its full potential, including IDEs and static analyzers.

These materials will discuss unique elements of the C++20 and newer standard that not only set this language apart from others, but that are not possible in other languages ​​or cannot be done safely and efficiently.

# Working environment

As support for these materials there is the following [Gitlab](https://gitlab.com/mobylabwebprogramming/saytencpp) where the mentioned examples can be found.

For those who want to work with this repository, we recommend using GCC 14.1 or newer and as IDE [CLion](https://www.jetbrains.com/clion/). The code on Gitlab includes a project that can be compiled with [CMake](https://cmake.org/download/), the de-facto tool for compiling C and C++. The project also contains a Dockerfile and a Docker Compose file. With [Docker](https://docs.docker.com/engine/install/), developers can have an isolated development environment that can be re-instantiated as needed, and CLion can connect to this environment via SSH.