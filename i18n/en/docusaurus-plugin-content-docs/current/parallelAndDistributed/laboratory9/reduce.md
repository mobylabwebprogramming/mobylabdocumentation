---
title: Reduce
sidebar_position: 1
---

Operația de reduce (cunoscută în limbajele funcționale - Haskell, Racket - drept fold) reprezintă un tip de operație prin care elementele unei colecții sunt restrânse / acumulate într-un singur rezultat, printr-o singură operație aplicată între elementele unei colecții (+, *, min, max etc.)

Exemplu:

```c showLineNumbers
l = [1, 2, 3, 4, 5, 6]
op = +
result = 1 + 2 + 3 + 4 + 5 + 6 = 21
```

In MPI, the reduce operation is implemented using the MPI_Reduce function. However, in this laboratory, we will discuss how this operation can be manually implemented in MPI.

Here are attached slides that provide detailed explanations of the steps involved in implementing the reduce operation: [slides](/files/parallel-and-distributed/reduce.pdf)

In conclusion, the first process will have the final value.

Pseudocode:

```c showLineNumbers
for (step = 2; step <= nr_processes; step *= 2)
	if (rank % step == 0)
		receive from the process with rank [rank + (step / 2)]
		add
        else if (rank % (step / 2) == 0)
            send to process with rank [rank - (step / 2)]
```