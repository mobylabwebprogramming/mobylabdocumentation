---
title: Exercises
sidebar_position: 6
---

[Lab Skeleton](https://github.com/APD-UPB/APD/tree/master/laboratoare/lab09).

1. Implement the reduction operation, where each process has its own value, resulting in a collection of values (do not use MPI_Reduction).
2. Implement the scan operation, where each process has its own value, resulting in a collection of values (do not use MPI_Scan).
3. Implement the broadcast operation, where process 0 sends a value to all processes (do not use MPI_Bcast).
4. Implement polynomial calculation using a pipeline for f(5), where f(x) represents a polynomial equation (constructed by reading from an existing file in the code skeleton, with hints provided for implementation). For testing, there are two test input files (a1.txt and a2.txt), and you can use either of them, with the caveat that the number of processes must be the same as the number of coefficients. For ease of testing, you can use the "run1" and "run2" rules from the Makefile.
5. Implement pipeline sorting using the laboratory skeleton, explanations from the lab, and slides.
6. Sort a vector using the Rank Sort algorithm. The master process will generate the vector, populate it with elements, and send the entire vector to other processes. Each process is responsible for calculating positions for a specific part of the vector. Finally, all processes will send their newly calculated positions back to the master process, which will rearrange the elements based on the received positions.

## Additional Resources

[Here](/files/parallel-and-distributed/lab9-suppliments.zip), you will find additional resources (slides) related to the concepts covered in this laboratory, which can be useful for practical tests.
