---
title: Scatter
sidebar_position: 4
---

## MPI_Scatter

MPI_Scatter is a function through which one process divides an array into equal-sized chunks, where each chunk is sent, in order, to each process, including itself, within the communicator.

The function signature is as follows:

```c showLineNumbers
int MPI_Scatter(void* send_data, int send_count, MPI_Datatype send_datatype, void* recv_data, int recv_count, MPI_Datatype recv_datatype, int root, MPI_Comm communicator)
```

Where:

* **send_data** (↓) - represents the data that is divided and sent to the processes within the communicator
* **send_count** (↓) - represents the size of the chunk that each process receives (usually set as total_size / number_of_processes).
* **send_datatype** (↓) - the data type of the data sent to processes
* **recv_data** (↑) - represents the data that is received and stored by processes
* **recv_count** (↓) - the size of the received data (usually total_size / number_of_processes)
* **recv_datatype** (↓) - the data type received by processes (usually the same as send_datatype)
* **root** (↓) - the identifier of the process that divides the data and sends it to the processes within the communicator, including itself
* **communicator** (↓) - the communicator to which the processes belong (usually **MPI_COMM_WORLD**)

An illustration of how MPI_Scatter works:

<img alt="img" src="/img/parallel-and-distributed/mpiScatter.png" width="50%" style={{margin: "auto", display: "block"}} />