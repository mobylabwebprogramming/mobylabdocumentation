---
title: Collections.synchronized Methods
sidebar_position: 5
---

If we want to work with classic collections like ArrayList and ensure synchronized operations, we can instantiate them using:

* Collections.synchronizedList (for lists):

```java showLineNumbers
List<Integer> syncList = Collections.synchronizedList(new ArrayList<>());
```

* Collections.synchronizedMap (for dictionaries):

```java showLineNumbers
Map<Integer, String> syncMap = Collections.synchronizedMap(new HashMap<>());
```