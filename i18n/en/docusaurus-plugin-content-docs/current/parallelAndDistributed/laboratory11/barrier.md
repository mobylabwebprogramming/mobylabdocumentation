---
title: Distributed Barrier
sidebar_position: 3
---

In MPI, there is the concept of a barrier, which is similar in functionality to that in pthreads and Java threads. Specifically, the barrier in MPI ensures that no process within the communicator can proceed beyond the point where the barrier is placed until all processes in the communicator reach that point.

Function signature (blocking version):

```c showLineNumbers
int MPI_Barrier(MPI_Comm comm)
```

Where the parameter 'comm' represents the MPI communicator in which the processes are running.

Additionally, there is a non-blocking version of the barrier in MPI:

```c showLineNumbers
int MPI_Ibarrier(MPI_Comm comm, MPI_Request *request)
```

Use case: I want to ensure that one process has finished writing before another one reads. Example of usage:

```c showLineNumbers
#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
 
int main(int argc, char **argv) {
 
    MPI_File out;
    int rank, numtasks;
    int ierr;
 
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
 
    remove("out.txt");
 
    ierr = MPI_File_open(MPI_COMM_WORLD, "out.txt", MPI_MODE_CREATE | MPI_MODE_RDWR, MPI_INFO_NULL, &out);
    if (ierr) {
        if (rank == 0) fprintf(stderr, "%s: Couldn't open output file %s\n", argv[0], argv[2]);
        MPI_Finalize();
        exit(1);
    }
 
 
    if (rank == 0) {
        char message_to_write[5] = "hello";
    	MPI_File_write_at(out, 0, message_to_write, 5, MPI_CHAR, MPI_STATUS_IGNORE);
    }
 
    MPI_Barrier(MPI_COMM_WORLD);
 
    if (rank == 1) {
    	char message_to_read[5];
    	MPI_File_read_at(out, 0, message_to_read, 5, MPI_CHAR, MPI_STATUS_IGNORE);
    	printf("\nMesajul citit este: ");
        for (int i = 0; i < 5; i++)
    	    printf("%c", message_to_read[i]);
        printf("\n");
    }
 
    MPI_File_close(&out);
 
    MPI_Finalize();
    return 0;
}
```

For reading and writing to the file, we have used the MPI_File_read_at and MPI_File_write_at functions. Both of these functions read from the file at a specified offset.

```c showLineNumbers
int MPI_File_read_at(MPI_File fh, MPI_Offset offset, void *buf, int count, MPI_Datatype datatype, MPI_Status *status)
```

:::tip
You can find more information about working with files in MPI here: [MPI I/O](/files/parallel-and-distributed/mpiIo.pdf).
:::