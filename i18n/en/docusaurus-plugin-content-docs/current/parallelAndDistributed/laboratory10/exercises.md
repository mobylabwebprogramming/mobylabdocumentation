---
title: Exercises
sidebar_position: 4
---

Starting from the [lab skeleton](https://github.com/APD-UPB/APD/tree/master/laboratoare/lab10), you will need to perform the following tasks:

1. Find the cluster leader node using a *heartbeat* algorithm (`leader_chosing` function in the skeleton).
2. Create the spanning tree starting from the leader using a *flood-fill* algorithm (`get_dst` function in the skeleton).
3. Ensure that the number of elements in the spanning tree has been accurately determined by calculating the number of nodes using an *epidemic* algorithm (`get_number_of_nodes` function in the skeleton).
4. Using the spanning tree, send the configuration of each node to the leader and create the topology matrix of the cluster in the leader. Distribute the topology to all nodes (`get_topology` function in the skeleton).