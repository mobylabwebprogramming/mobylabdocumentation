---
title: Parallel and Distributed Algorithms
sidebar_position: 1
---

<img alt="img" src="/img/parallel-and-distributed/APD.png" width="400" style={{margin: "auto", display: "block"}} />

# About

The "Parallel and Distributed Algorithms" course provides the necessary skills for solving problems through parallel or distributed solutions. It covers fundamental concepts and models of parallel and distributed programming, methods for designing parallel and distributed solutions, ways to implement solutions using specific programming languages or libraries, and methods for improving performance using complexity models, etc.

Students will acquire skills related to the design and implementation of parallel and distributed algorithms, as well as the analysis and selection of appropriate solutions for creating software products using distributed systems technologies.

The course also aims to establish a solid foundation (both in theory and practical applications) regarding parallel and distributed algorithms. Upon completing the course, students will be able to design correct solutions that incorporate parallel and distributed algorithms and critically evaluate the complexity properties of various applications. Additionally, they will understand the available alternatives for different designed systems.

# Team

## Course

- CA series: Ciprian Dobre
- CB series: Elena Apostol
- CC series: Radu-Ioan Ciobanu
- CD series: Dorinel Filip

## Laboratory

- Radu-Ioan Ciobanu
- Dorinel Filip
- Gabriel Guțu-Robu
- Silviu Pantelimon
- Valentin Bercaru
- Florin Mihalache
- Diana-Mihaela Megelea
- Lucian Iliescu
- Lucian Oprea
- Cătălin-Lucian Picior
- Cosmin-Viorel Lovin
- Catalin-Alexandru Rîpanu
- Andreea Borbei
- Alexandra-Ana-Maria Ion
- George-Alexandru Tudor
- Costin-Alexandru Deonise
- Taisia-Maria Coconu
- Dragoș Sofia
- Alexandru-Petruț Matei
- Marius-Alexandru Manolache
- Artemiza Dospinescu
- Bogdan-Mihai Butnariu
- Andrei-Alexandru Podaru
- Vlad Juja
- Sarah-Maria Crăciun
- George-Florin Vasilache

# Schedule

## Course

**TBD**

* **CA**: Monday 14-16 (EC105), Wednesday 14-16 odd (PR001) 
* **CB**: Monday 8-10 (PR001), Thursday 16-18 odd (PR001)
* **CC**: Wednesday 16-18 (PR001), Thursday 16-18 even (PR001)
* **CD**: Tuesday 16-18 (EG301), Friday 12-14 even (AN034)

## Lab

| **ED120** 	| **Luni** 	| **Marți** 	| **Miercuri** 	| **Joi** 	| **Vineri** 	|
|:---------:	|:--------:	|:---------:	|:------------:	|:-------:	|:----------:	|
|    8-10   	|          	|   331CCa  	|    333CDa    	|         	|   332CBa   	|
|   10-12   	|          	|   333CCb  	|    334CDa    	|         	|   331CBa   	|
|   12-14   	|          	|   334CCa  	|    333CCa    	|         	|            	|
|   14-16   	|          	|           	|              	|         	|            	|
|   16-18   	|          	|           	|              	|         	|   332CDb   	|
|   18-20   	|          	|           	|              	|         	|            	|
| **EG205** 	| **Luni** 	| **Marți** 	| **Miercuri** 	| **Joi** 	| **Vineri** 	|
|    8-10   	|  331CAa  	|   333CAb  	|    333CBb    	| 331CDa  	|   331CAb   	|
|   10-12   	|  332CAa  	|   334CBb  	|    334CAa    	|         	|   335CAa   	|
|   12-14   	|  334CCb  	|   332CAb  	|    333CDb    	|  332CBb 	|            	|
|   14-16   	|  334CDb  	|   335CCb  	|    334CBa    	|  331CBb 	|   333CAa   	|
|   16-18   	|  331CCb  	|   332CCa  	|    334CAb    	|  331CDb 	|   335CAb   	|
|   18-20   	|  332CCb  	|   335CCa  	|    333CBa    	|  332CDa 	|            	|