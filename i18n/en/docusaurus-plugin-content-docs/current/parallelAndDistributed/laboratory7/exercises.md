---
title: Exercises
sidebar_position: 3
---
:::tip
In this lab, you will work with [this skeleton](https://github.com/APD-UPB/APD/tree/master/laboratoare/lab07), which you will use to parallelize three problems based on the Replicated Workers model, using ExecutorService and ForkJoinPool:

* finding paths between nodes in a graph
* graph coloring → find additional details [here](/files/parallel-and-distributed/graphColoring.pdf).
* queens problem → find additional details [here](/files/parallel-and-distributed/queensProblem.pdf).
:::

1. Parallelize the finding of paths between two nodes using the provided skeleton (in the *task1* package) using ExecutorService.
2. Parallelize the graph coloring problem using the provided skeleton (in the *task2* package) using ExecutorService.
3. Parallelize the queens problem using the provided skeleton (in the *task3* package, with solutions *[(2, 1), (4, 2), (1, 3), (3, 4)]* and *[(3, 1), (1, 2), (4, 3), (2, 4)]*) using ExecutorService.
4. Parallelize the finding of paths between two nodes using the provided skeleton (in the *task4* package) using ForkJoinPool.
5. Parallelize the graph coloring problem using the provided skeleton (in the *task5* package) using ForkJoinPool.
6. Parallelize the queens problem using the provided skeleton (in the *task6* package) using ForkJoinPool.