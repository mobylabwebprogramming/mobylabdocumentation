document.addEventListener('keydown', function(e) {
    if (e.key === 'ArrowLeft') {
      const buttons = document.getElementsByClassName('pagination-nav__link--prev');
      if (buttons.length === 0)
        return;
      buttons[0].click();
    } else if (e.key === 'ArrowRight') {
      const buttons = document.getElementsByClassName('pagination-nav__link--next');
      if(buttons.length === 0)
        return;
      buttons[0].click();
    }
  })
