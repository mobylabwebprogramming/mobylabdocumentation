---
title: Exerciții
sidebar_position: 5
---

1. Pornind de la implementarea secvențială a algoritmului bubble sort aflată în fișierul **oets.c** din [scheletul de laborator](https://github.com/APD-UPB/APD/tree/master/laboratoare/lab03), implementați și paralelizați algoritmul odd-event transposition sort.
2. Analizați scalabilitatea implementării de la punctul precedent. Verificați **sugestia 1** de mai jos pentru informații suplimentare.
3. Pornind de la implementarea secvențială a algoritmului shear sort aflată în fișierul **shear.c** din scheletul de laborator, paralelizați algoritmul.
4. Analizați scalabilitatea implementării de la punctul precedent. Verificați **sugestia 1** de mai jos pentru informații suplimentare.
5. Pornind de la implementarea secvențială a algoritmului merge sort aflată în fișierul **merge.c** din scheletul de laborator, paralelizați algoritmul. Verificați **sugestia 2** de mai jos pentru informații suplimentare.
6. Pornind de la scheletul de cod din fișierul **parallel_binary_search.c** din scheletul de laborator, care are o implementare secvențială a algoritmului de căutare binară ca hint, implementați versiunea paralelizată a căutarii binare. Analizați scalabilitatea implementării.

:::tip
1. În toate fișierele sursă din scheletul de laborator, verificarea corectitudinii implementării paralele se realizează prin sortarea șirului folosind quick sort într-un array folosit ca etalon la final (se realizează o comparație element cu element). Atunci când doriți să verificați scalabilitatea programului vostru, comentați sau eliminați sortarea folosind quick sort și comparația aferentă, pentru a avea niște timpi cât mai corecți. De asemenea, încercați să eliminați toate afișările în terminal.
:::

:::tip
2. Implementarea merge sort din scheletul de laborator funcționează pentru un N putere a lui 2. Testați-vă implementarea paralelă doar pe acest caz.
:::
