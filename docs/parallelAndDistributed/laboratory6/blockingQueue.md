---
title: Clasa BlockingQueue
sidebar_position: 4
---

Interfaţa [BlockingQueue](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/util/concurrent/BlockingQueue.html) extinde interfaţa Queue, clasele care o implementează fiind potrivite pentru programe paralele, în care pot exista probleme de concurenţă. Dintre acestea, cele mai relevante sunt ArrayBlockingQueue, LinkedBlockingQueue şi PriorityBlockingQueue. Toate simulează o structură de tip FIFO (coadă), cu operaţii thread-safe.

Diferenţa este la implementarea din spate: ArrayBlockingQueue e bazată pe un array, LinkedBlockingQueue pe o listă înlănţuită, iar pentru PriorityBlockingQueue poate fi specificat un comparator la iniţializare, păstrând o ordine în care să fie elementele. Niciuna dintre clase nu acceptă elemente de tip *null* şi li se poate specifica o capacitate maximă la iniţializare (pentru ArrayBlockingQueue este obligatoriu acest lucru).

Operaţiile sunt pe principiul FIFO (se inserează la final şi se extrage de la începutul cozii), așa cum se poate observa în tabelul de mai jos.

| Operație  | Aruncă excepție | Valoare specială | Se blochează | Dă timeout           |
|:---------:|:---------------:|:----------------:|:------------:|:--------------------:|
| Inserare  | add(e)          | offer(e)         | put(e)       | offer(e, time, unit) |
| Ștergere  | remove()        | poll()           | take()       | poll(time, unit)     |
| Examinare | element()       | peek()           |              |                      |

Deşi sunt mai multe metode pentru aceeaşi operaţie asemănătoare, comportamentul lor este diferit. Cea mai importantă diferenţă o constituie proprietatea de a fi sau nu blocantă. De exemplu, **add(obj)** adaugă un element doar dacă este loc în coadă (adică dacă nu s-a atins capacitatea maximă) şi returnează true dacă operaţia a reuşit, sau **IllegalStateException** în caz contrar. În schimb, **put(obj)** se blochează dacă nu este spaţiu suficient în coadă şi aşteaptă până poate pune elementul cu succes.