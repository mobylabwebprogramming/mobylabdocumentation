---
title: Exerciții
sidebar_position: 3
---
:::tip
În acest laborator, veți lucra cu [acest schelet](https://github.com/APD-UPB/APD/tree/master/laboratoare/lab07), pe care îl veți utiliza să paralelizați trei probleme pe baza modelului Replicated Workers, folosind ExecutorService și ForkJoinPool:

* găsirea căilor dintre nodurile unui graf
* colorarea unui graf → găsiți detalii adiționale [aici](/files/parallel-and-distributed/graphColoring.pdf).
* problema damelor → găsiți detalii adiționale [aici](/files/parallel-and-distributed/queensProblem.pdf).
:::

1. Paralelizați găsirea căilor între două noduri pe baza scheletului oferit (în pachetul *task1*) folosind ExecutorService.
2. Paralelizați problema colorării unui graf pe baza scheletului oferit (în pachetul *task2*) folosind ExecutorService.
3. Paralelizați problema damelor pe baza scheletului oferit (în pachetul *task3*, cu soluțiile *[(2, 1), (4, 2), (1, 3), (3, 4)]* și *[(3, 1), (1, 2), (4, 3), (2, 4)]*) folosind ExecutorService.
4. Paralelizați găsirea căilor între două noduri pe baza scheletului oferit (în pachetul *task4*) folosind ForkJoinPool.
5. Paralelizați problema colorării unui graf pe baza scheletului oferit (în pachetul *task5*) folosind ForkJoinPool.
6. Paralelizați problema damelor pe baza scheletului oferit (în pachetul *task6*) folosind ForkJoinPool.