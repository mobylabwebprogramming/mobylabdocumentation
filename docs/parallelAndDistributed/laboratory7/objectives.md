---
title: Obiectiv și studiu de caz
sidebar_position: 0
---

În cadrul acestui laborator, vom utiliza modelul **Replicated Workers** și vom studia și utiliza mai multe interfețe Java care permit implementarea acestui model care poate fi folosit inclusiv pentru paralelizarea algoritmilor descriși ca funcții recursive.

În exemplificarea utilizării fiecăreia dintre interfețe, vom prezenta implementarea paralelă a următoarea funcții recursive:

```java showLineNumbers
import java.io.File;

public class Main {
    public static void show(String path) {
        File file = new File(path);
        if (file.isFile()) {
            System.out.println(file.getPath());
        } else if (file.isDirectory()) {
            var files = file.listFiles();
            if (files != null) {
                for (var f : files) {
                    show(f.getPath());
                }
            }
        }
    }

    public static void main(String[] args) {
        show("files");
    }
}
```

Se observă ușor că funcția afișează (recursiv) toate fișierele dintr-o ierarhie de directoare. O decizie importantă în această implementare o reprezintă alegerea dimensiunii taskurilor pe care le vom rula în paralel. Pentru simplitate, vom crea un nou task pentru fiecare apel recursiv.