---
title: Exerciții
sidebar_position: 5
---

Scheletul de laborator este disponibil [aici](https://github.com/APD-UPB/APD/tree/master/laboratoare/lab08).

* **Exercițiul 1**: Implementați algoritmul inel folosind MPI, unde procesul cu id-ul 0 trimite un număr aleatoriu către procesul 1 (vecinul său), iar apoi celelalte noduri vor primi numărul de la procesul precedent, îl incrementează cu 2 și îl trimit la următorul proces (de exemplu procesul 2 primește valoarea de la procesul 1, o incrementează și o trimite mai departe procesului 3), totul terminându-se când valoarea ajunge la procesul 0. Pentru fiecare proces trebuie să afișați rangul acestuia și valoarea primită.
* **Exercițiul 2**: Implementați un program în MPI în care procesul 0 trimite o valoare generată aleatoriu către celelalte procese, folosind **MPI_Bcast**. **Atenție**: Nu folosiți **MPI_Recv**.
* **Exercițiul 3**: Implementați un program în MPI cu N procese, în care procesul 0 inițializează un array de dimensiune 5 * N cu 0, îl împarte (dimensiunea chunk-ului fiind 5, definită în schelet) și îl trimite tuturor proceselor cu **MPI_Scatter**. Procesele vor aduna fiecare element primit cu rangul fiecăruia, apoi ele vor trimite array-urile pe care au efectuat operația de adunare cu rangul către procesul 0 folosind **MPI_Gather**. Procesul 0 va afișa ulterior array-ul rezultat din **MPI_Gather**.
* **Exercițiul 4**: Implementați un program MPI cu 4 procese, unde 3 procese trimit o valoare către al patrulea proces, care va primi valoarea folosind **MPI_ANY_SOURCE** și va afișa valoarea primită și rangul procesului sursă folosind **MPI_SOURCE** din **MPI_Status** (status din **MPI_Recv**).

:::tip
**MPI_ANY_SOURCE** este folosit în loc de rangul procesului sursă la **MPI_Recv**, indicând astfel că procesul receptor poate primi un mesaj de la oricare proces.
:::

* **Exercițiul 5**: Implementați un program MPI cu 2 procese, unde procesul 0 trimite 10 valori cu tag-uri diferite către procesul 1, iar procesul 1 primește valorile folosind parametrul **MPI_ANY_TAG** în **MPI_Recv**. Afișați valoarea primită împreună cu tag-ul acesteia folosind **MPI_Status** (status din **MPI_Recv**).

:::tip
**MPI_ANY_TAG** este folosit în loc de tag-ul mesajului la **MPI_Recv**, indicând faptul că poate primi orice mesaj de la procesul sursă, neținând cont de tag-ul pe care procesul sursă îl pune mesajului la **MPI_Send**.
:::

* **Exercițiul 6**: Se pornește un număr de procese N divizibil cu 4. Se împart procesele în grupe de câte 4 și își trimit în cerc (inel) rangul. Se afișează vechiul rang împreună cu dimensiunea vechiului grup. Se afișează noul rang împreună cu dimensiunea noului grup. Fiecare proces are voie sa comunice doar în grupul lui. Se va folosi **MPI_Comm_split**. Fiecare proces va afișa rangul său din noul grup, numărul grupului și ce număr a primit.