---
title: Problema filozofilor
sidebar_position: 2
---

Problema se referă la mai mulți filozofi (thread-uri) așezați la o masă circulară. Pe masă se află 5 farfurii și 5 tacâmuri, astfel încât fiecare filozof are un tacâm în stânga și unul în dreapta lui. În timp ce stau la masă, filozofii pot face două acțiuni: mănâncă sau se gândesc. Pentru a mânca, un filozof are nevoie de două tacâmuri (pe care le poate folosi doar dacă nu sunt luate de către vecinii săi).

Rezolvarea trebuie să aibă în vedere dezvoltarea unui algoritm prin care să nu se ajungă la un deadlock (situația în care fiecare filozof ține câte un tacâm în mână și așteaptă ca vecinul să elibereze celălalt tacâm de care are nevoie).

Ca soluție, avem în felul următor: vom avea N lock-uri (având în vedere că avem N thread-uri), fiecare filosof va folosi câte două lock-uri. Pentru a evita deadlock-ul, totul va funcționa în felul următor:

* fiecare din primele N - 1 thread-uri va face lock mai întâi pe lock pe lock[i], apoi pe lock[i + 1], apoi execută o acțiune, apoi face release pe lock[i], apoi pe lock[i + 1].
* al N-lea thread va face lock mai întâi pe lock[0], apoi pe lock[N - 1] (deci invers față de restul thread-urilor), execută o acțiune, apoi face release pe lock[0], apoi pe lock[N - 1].

Pseudocod:

```c showLineNumbers
Lock[] locks = new Lock[N];

philosopher(int id) {
    while (true) {
        if (id != N - 1) {
	        locks[id].lock();
	        locks[id + 1].lock();
	        // eat
	        locks[id].release();
	        locks[id + 1].release();
	        // think
	    } else {
	        locks[0].lock();
	        locks[N - 1].lock();
	        // eat
	        locks[0].release();
	        locks[N - 1].release();
	        // think
	    }
    }
}
```

<a href="/files/parallel-and-distributed/diningPhilosophers.pdf" target="_blank">CheatSheet Dining Philosophers</a>