---
title: Deadlock și livelock
sidebar_position: 3
---

Un deadlock reprezintă o situație când thread-urile sunt blocate, fiecare thread dorind să acceseze resursele ocupate de alt thread în același timp, și poate fi exemplificat în felul următor: avem două thread-uri, T1 și T2, T1 deține un lock P și T2 deține un lock Q, iar T1 vrea să obțină lock-ul Q, care e luat de T2, și T2 vrea să obțină lock-ul P, care e deținut de T1.

Un livelock are loc atunci când două thread-uri fac acțiuni în așa mod o acțiune o blochează pe cealaltă și viceversa, cele două fiind dependente una de cealaltă, astfel task-urile executate de aceste thread-uri nu pot fi terminate. Spre deosebire de deadlock, thread-urile nu sunt blocate, acțiunea a unui thread răspunzând la acțiunea celuilalt thread și invers.