---
title: Clasa CyclicBarrier
sidebar_position: 3
---

Bariera ciclică reprezintă un mecanism de (re)sincronizare a mai multor thread-uri care are scopul de a bloca un număr specificat de thread-uri și de a le lăsa sa își continue execuția într-un mod sincron doar după ce toate au apelat metoda acesteia de resincronizare. În Java, acest mecanism este reprezentat de clasa CyclicBarrier. La instanțierea acesteia, se va specifica numărul de thread-uri pe care aceasta le va resincroniza. Acest mecanism de sincronizare a thread-urilor este util în cazul algoritmilor iterativi ce sunt rulați în paralel și au nevoie de o etapă de resincronizare a firelor de execuție înainte de a trece la noua iterație de calcul. Trebuie ținut cont de faptul că apelul metodei **await()** pe bariera ciclică poate arunca o excepție de tipul **BrokenBarrierException** sau **InterruptedException**.

```java showLineNumbers
public class Task extends Thread {
 
    private int id;
 
    public Task(int id) {
        this.id = id;
    }
 
    public void run() {
        while (!solved) {
            executeAlgorithmStep();
 
            try {
                //Resincronizarea thread-urilor pentru urmatorul pas al algoritmului.
                Main.barrier.await();
            } catch (BrokenBarrierException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
```

```java showLineNumbers
public class Main {
 
    public static CyclicBarrier barrier;
 
    public static void main(String[] args) {
        int NUMBER_OF_THREADS = 4;
        barrier = new CyclicBarrier(NUMBER_OF_THREADS);
        Task[] t = new Task[NUMBER_OF_THREADS];
 
        for (int i = 0; i < NUMBER_OF_THREADS; ++i) {
            t[i] = new Task(id);
            t[i].start();
        }
 
        for (int i = 0; i < NUMBER_OF_THREADS; ++i) {
            try {
                t[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
```

<a href="/files/parallel-and-distributed/cyclicBarrier.pdf" target="_blank">CheatSheet CyclicBarrier</a>
