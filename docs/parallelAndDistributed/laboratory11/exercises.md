---
title: Exerciții
sidebar_position: 5
---

[Schelet laborator](https://github.com/APD-UPB/APD/tree/master/laboratoare/lab11)

1. Implementați algoritmul inel folosind folosind funcții non-blocante, folosind scheletul din *ring.c*, unde procesul cu id-ul 0 trimite un număr aleatoriu către procesul 1 (vecinul său), iar apoi celelalte noduri vor primi numărul de la procesul precedent, îl incrementează cu 2 și îl trimit la următorul proces (de exemplu procesul 2 primește valoarea de la procesul 1, o incrementează și o trimite mai departe procesului 3), totul terminându-se când valoarea ajunge la procesul 0. Pentru fiecare proces trebuie să afișați rangul acestuia și valoarea primită (hint: primul exercițiu din laboratorul 8).
2. Rezolvați deadlock-ul din fișierul deadlock.c din scheletul de laborator în 3 variante (verificați și hint-ul de mai jos):
* folosind **MPI_Sendrecv**
* folosind **MPI_Bsend**
* folosind funcții non-blocante - aici în loc de **MPI_Test** și **MPI_Wait** folosiți [MPI_Waitall](https://rookiehpc.github.io/mpi/docs/mpi_waitall/index.html).
3. Plecând de la scheletul de cod din *queue.c*, unde rulează procesele în cadrul unei topologii inel, creați un nou tip de date în MPI, plecând de la structura definită în cod. Fiecare proces va genera un număr random, care va fi adăugat în structura respectivă, la final procesul 0 având fiecare număr generat de fiecare proces. După aceea, procesul 0 va afișa ce elemente se află în structura respectivă, care este o structură de tip coadă.

**Bonus**. Pornind de la schelet, folosiți bariera în fișierul *barrier.c* pentru a vă asigura că în fișier output-ul este mereu **hello**.

:::note
Pentru exercițiul 2 puteți să vă folosiți de acest [hint](http://www.idris.fr/eng/su/shared/mpi_send_recv-eng.html).
:::

:::tip
Adăugarea în coadă se face în felul următor: q.arr[q.size++] = element;
:::