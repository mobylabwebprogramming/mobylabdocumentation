---
title: Algoritmi Paraleli și Distribuiți
sidebar_position: 1
---

<img alt="img" src="/img/parallel-and-distributed/APD.png" width="400" style={{margin: "auto", display: "block"}} />

# Despre

Materia "Algoritmi Paraleli și Distribuiți" formează competențe necesare pentru rezolvarea problemelor prin soluții paralele sau distribuite. Se vor aborda concepte de bază și modele de programare paralelă și distribuită, metode de proiectare a soluțiilor paralele și distribuite, modalități de implementare a soluțiilor folosind limbaje de programare sau biblioteci specifice, metode de îmbunătățire a performanței soluțiilor folosind modele de complexitate, etc.

Studenții vor căpăta deprinderi legate de proiectarea și implementarea algoritmilor paraleli și distribuiți, dar și de analiza și alegerea soluțiilor potrivite pentru crearea de produse software folosind tehnologii de sisteme distribuite.

Materia urmărește, de asemenea, dobândirea unui fundament solid (teorie și aplicații practice) privind algoritmii paraleli și distribuiți. După absolvirea materiei, studenții vor putea să proiecteze soluții corecte în care sunt aplicați algoritmi paraleli și distribuiți, precum și să evalueze critic proprietățile de complexitate ale unor aplicații, înțelegând totodată alternativele existente pentru diverse sisteme proiectate.

# Echipa

## Curs

- Seria CA: Ciprian Dobre
- Seria CB: Elena Apostol
- Seria CC: Radu-Ioan Ciobanu
- Seria CD: Dorinel Filip

## Laborator

- Radu-Ioan Ciobanu
- Dorinel Filip
- Gabriel Guțu-Robu
- Silviu Pantelimon
- Valentin Bercaru
- Florin Mihalache
- Diana-Mihaela Megelea
- Lucian Iliescu
- Lucian Oprea
- Cătălin-Lucian Picior
- Cosmin-Viorel Lovin
- Catalin-Alexandru Rîpanu
- Andreea Borbei
- Alexandra-Ana-Maria Ion
- George-Alexandru Tudor
- Costin-Alexandru Deonise
- Taisia-Maria Coconu
- Dragoș Sofia
- Alexandru-Petruț Matei
- Marius-Alexandru Manolache
- Artemiza Dospinescu
- Bogdan-Mihai Butnariu
- Andrei-Alexandru Podaru
- Vlad Juja
- Sarah-Maria Crăciun
- George-Florin Vasilache

# Orar

## Curs

* **CA**: Luni 14-16 (EC105), Miercuri 14-16 impar (PR001) 
* **CB**: Luni 8-10 (PR001), Joi 16-18 impar (PR001)
* **CC**: Miercuri 16-18 (PR001), Joi 16-18 par (PR001)
* **CD**: Marți 16-18 (EG301), Vineri 12-14 par (AN034)

## Laborator

| **ED120** 	| **Luni** 	| **Marți** 	| **Miercuri** 	| **Joi** 	| **Vineri** 	|
|:---------:	|:--------:	|:---------:	|:------------:	|:-------:	|:----------:	|
|    8-10   	|          	|   331CCa  	|    333CDa    	|         	|   332CBa   	|
|   10-12   	|          	|   333CCb  	|    334CDa    	|         	|   331CBa   	|
|   12-14   	|          	|   334CCa  	|    333CCa    	|         	|            	|
|   14-16   	|          	|           	|              	|         	|            	|
|   16-18   	|          	|           	|              	|         	|   332CDb   	|
|   18-20   	|          	|           	|              	|         	|            	|
| **EG205** 	| **Luni** 	| **Marți** 	| **Miercuri** 	| **Joi** 	| **Vineri** 	|
|    8-10   	|  331CAa  	|   333CAb  	|    333CBb    	| 331CDa  	|   331CAb   	|
|   10-12   	|  332CAa  	|   334CBb  	|    334CAa    	|         	|   335CAa   	|
|   12-14   	|  334CCb  	|   332CAb  	|    333CDb    	|  332CBb 	|            	|
|   14-16   	|  334CDb  	|   335CCb  	|    334CBa    	|  331CBb 	|   333CAa   	|
|   16-18   	|  331CCb  	|   332CCa  	|    334CAb    	|  331CDb 	|   335CAb   	|
|   18-20   	|  332CCb  	|   335CCa  	|    333CBa    	|  332CDa 	|            	|