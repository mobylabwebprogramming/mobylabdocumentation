---
title: Exerciții
sidebar_position: 6
---

[Schelet laborator](https://github.com/APD-UPB/APD/tree/master/laboratoare/lab09).

1. Implementați operația de reduction, unde fiecare proces are o valoare proprie, prin care avem o colecție de valori (a nu se folosi MPI_Reduction)
2. Implementați operația de scan, unde fiecare proces are o valoare proprie, prin care avem o colecție de valori (a nu se folosi MPI_Scan)
3. Implementați operația de broadcast, prin care procesul 0 trimite o valoare tuturor proceselor (a nu se folosi MPI_Bcast)
4. Implementați operația de calcul polinomial folosind pipeline, pentru f(5), unde f(x) reprezintă ecuația polinomială (construită prin citirea din fișier existentă în scheletul de cod, unde aveți hint-uri legate de implementare). Pentru testare, există două fișiere de test pentru input (a1.txt și a2.txt) și puteți să vă folosiți de oricare dintre ele, cu precizia că numărul de procese trebuie să fie același cu numărul de coeficienți. Pentru ușurință în testare, puteți folosi regulile run1 și run2 din Makefile.
5. Implementați sortarea prin pipeline, folosind scheletul de laborator și explicațiile din laborator și slide-uri
6. Să se sorteze un vector folosindu-se algoritmul Rank Sort. Procesul master va genera vectorul și îl va popula cu elemente. Va trimite tot vectorul celorlalte procese. Fiecare proces este responsabil pentru calcularea pozițiilor unei anumite părți din vector. La final toate procesele trimit procesului master noile poziții calculate. Procesul master va aranja elementele conform noilor poziții primite.

## Resurse suplimentare

[Aici](/files/parallel-and-distributed/lab9-suppliments.zip) aveți atașate resurse suplimentare (slide-uri) legate de conceptele din cadrul acestui laborator, ce pot fi utile pentru testele practice.