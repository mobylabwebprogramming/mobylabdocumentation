---
title: C++ Modern
sidebar_position: 1
---

## Contribuitor
[Silviu-George Pantelimon](https://teams.microsoft.com/l/chat/0/0?users=silviu.pantelimon@upb.ro)

# Disclaimer

Acesta secțiune este încă în lucru.

# Introducere

Acesta este o introducere în aspecte interesante din C++ modern. De ceva timp se pune problema dacă C++ este un limbaj de actualitate sau nu. Multe din problemele limbajului C++ sunt legate de nesiguranță când vine vorba de lucratul cu memoria direct, ceea ce poate conduce la erori și risipă de resurse computaționale sau de complexitatea limbajului.

Însă adevărul este că aceste probleme sunt mai mult de natură umană, din cauza faptului că programatorii nu știu să lucreze corect cu limbajul și să-l înțeleagă. De asemenea, anumite probleme pot fi legate și de faptul că limbajul este destul de vechi, fiind lansat în 1985. Dar vechi nu înseamnă neapărat învechit. Limbajul a evoluat constant, iar standardul a fost extins pentru a fi în pas cu necesitățile actuale ale programatorilor. Acum, programatorii au la dispoziție mai multe utilitare pentru a folosi limbajul la capacitate maximă, inclusiv IDE-uri și analizatoare statice.

Aceste materiale vor discuta despre elemente unice ale standardului C++20 și mai nou, care nu doar că diferențiază acest limbaj față de altele, dar care nu sunt posibile în alte limbaje sau nu se pot face în mod sigur și eficient.

# Mediu de lucru

Ca suport pentru aceste materiale există următorul [Gitlab](https://gitlab.com/mobylabwebprogramming/saytencpp) unde se pot găsi exemplele menționate.

Pentru cei care doresc să lucreze cu acest repository, recomandăm folosirea GCC 14.1 sau mai nou și ca IDE [CLion](https://www.jetbrains.com/clion/). Codul de pe Gitlab include un proiect care poate fi compilat cu [CMake](https://cmake.org/download/), utilitarul de-facto pentru compilarea C și C++. De asemenea, proiectul conține un Dockerfile și un fișier Docker Compose. Cu [Docker](https://docs.docker.com/engine/install/), dezvoltatorii pot avea un mediu de dezvoltare izolat care poate fi reinstanțiat la nevoie, iar CLion se poate conecta la acest mediu prin SSH.