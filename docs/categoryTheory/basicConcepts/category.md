---
title: Categorie
sidebar_position: 1
---

# Termeni

Pentru a defini ce este o categorie, trebuie să clarificăm câțiva termeni. În multe cazuri, vom folosi termenul de **mulțime (set)**, o colecție de elemente unice, și **clasă (class)** pentru colecții de mulțimi. Utilizarea termenului de clasă poate depinde de contextul fondațional, cum ar fi teoria mulțimilor Zermelo-Fraenkel sau teoria mulțimilor von Neumann–Bernays–Gödel. Pentru simplitate, vom folosi termenul de mulțime dacă elementele sale nu au proprietăți suplimentare și termenul de clasă dacă elementele reprezintă structuri cu proprietăți. Astfel, putem defini ce este o categorie.

## Definiție: Categorie

O categorie $C$ este compusă din:

- o mulțime $Ob(C)$ ale cărei elemente se numesc **obiecte**
- o mulțime $Hom(C)$ ale cărei elemente se numesc **săgeți** sau **morfisme**
- pentru fiecare morfism $f \in Hom(C)$ există o pereche de obiecte $s(f), t(f) \in Ob(C)$ numite **domeniul**, respectiv **codomeniul** lui $f$
- pentru fiecare pereche de morfisme $f,g \in Hom(C)$ unde $t(f) = s(g)$ există mereu un morfism $g \circ f \in Hom(C)$ (citit $g$ după $f$) numit **compusul**
- pentru fiecare obiect $x \in Ob(C)$ există mereu un morfism $id_x$
- astfel încât următoarele proprietăți sunt satisfăcute:
  - sursa și destinația respectă compoziția: $s(g \circ f) = s(f)$ și $t(g \circ f) = t(g)$
  - sursa și destinația respectă identitatea: $s(id_x) = x$ și $t(id_x) = x$
  - compoziția e asociativă: $(h \circ g) \circ f = h \circ (g \circ f)$
  - compoziția satisface unitatea stânga și dreapta: $id_{t(f)} \circ f = f \circ id_{s(f)} = f$

Un exemplu de categorie ar putea fi următorul:

```mermaid
flowchart TD
    subgraph Categorie
        a((a)) -->|id<sub>a</sub>| a((a))
        a((a)) -->|f| b((b))
        b((b)) -->|id<sub>b</sub>| b((b))
        a((a)) -->|g∘f| c((c))
        b((b)) -->|g| c((c))
        c((c)) -->|id<sub>c</sub>| c((c))
    end
```

În principiu, o categorie este un quiver, un graf orientat cu oricâte muchii între oricare două noduri. Aici, importante sunt două proprietăți ale categoriei: faptul că compunerea morfismelor este asociativă și păstrează identitatea. Aceste proprietăți vor fi folosite în conceptul următor, care are legătură mai mult cu programarea funcțională, anume **functorul**.

# Resurse

* Alexander Grothendieck, Section 4 of: Techniques de construction et théorèmes d’existence en géométrie algébrique III: préschémas quotients, Séminaire Bourbaki: années 1960/61, exposés 205-222, Séminaire Bourbaki, no. 6 (1961), Exposé no. 212, [link](http://www.numdam.org/item/?id=SB_1960-1961__6__99_0)
* [ncatlab](https://ncatlab.org/nlab/show/category)