---
title: Functor
sidebar_position: 2
---

Programatorii sunt obișnuiți cu conceptul de **functor** și pot recunoaște tipuri generice, cum sunt listele, ca functori în limbajele orientate pe obiecte, deoarece de obicei au o metodă **map/fmap/select** care pot transforma o listă de tip `List<int>` într-o listă `List<string>` printr-o funcție $$f : int \rightarrow string$$.

Adevărul este că conceptul de functor din matematică și cel din programare reprezintă același concept din perspective diferite. Corespondența nu este întotdeauna intuitivă și evidentă, iar în continuare vom arăta de ce.

## Definiție: Functor

Un **functor** $F$ de la o **categorie** $C$ la alta $D$, notat $F : C \rightarrow D$, este o mapare care trimite $\forall x \in Ob(C)$ într-un obiect $F(x) \in Ob(D)$ și care trimite $\forall f \in Hom(C)$ într-un morfism $F(f) \in Hom(D)$, astfel încât:
* $F$ conservă compoziția: $F(h \circ g) = F(h) \circ F(g)$
* $F$ conservă identitatea: $F(id_{x}) = id_{F(x)}$

Practic, un functor este un **homomorfism** (mapare care conservă structura) între două categorii.

# Legătura cu programarea

Revenind la corespondența între programare și matematică, putem evidenția legătura considerând următorul exemplu. Să presupunem că, în cadrul unui program, avem trei tipuri de date: **int**, **bool** și **string**, cu următoarele funcții între ele: $toString : int \rightarrow string$ și $isOdd : int \rightarrow bool$. Să presupunem că tipurile noastre de bază reprezintă o categorie, cu tipurile fiind obiecte, iar funcțiile morfisme. Dacă am introduce un functor numit $List$ ca un tip generic care are o funcție $map : List\ a \rightarrow (a \rightarrow b) \rightarrow List\ b$, atunci tuplul $(List, map)$ reprezintă functorul conform definiției date, ca cele două mapări pe obiecte, respectiv morfisme.

```mermaid
flowchart LR
    int((int)) -.->ListInt((List&lt;int&gt;))
    string((string)) -.->ListString((List&lt;string&gt;))
    bool((bool)) -.->ListBool((List&lt;bool&gt;))
    subgraph Cat [Categorie]
        int((int)) -->|id<sub>int</sub>| int((int))
        int((int)) -->|toString| string((string))
        string((string)) -->|id<sub>string</sub>| string((string))
        int((int)) -->|isOdd| bool((bool))
        bool((bool)) -->|id<sub>bool</sub>| bool((bool))
    end
    subgraph ListCat [Noua categorie]
        ListInt((List&lt;int&gt;)) -->|map id<sub>int</sub>| ListInt((List&lt;int&gt;))
        ListInt((List&lt;int&gt;)) -->|map toString| ListString((List&lt;string&gt;))
        ListString((List&lt;string&gt;)) -->|map id<sub>string</sub>| ListString((List&lt;string&gt;))
        ListInt((List&lt;int&gt;)) -->|map isOdd| ListBool((List&lt;bool&gt;))
        ListBool((List&lt;bool&gt;)) -->|map id<sub>bool</sub>| ListBool((List&lt;bool&gt;))
    end
    Cat ---->|List| ListCat
```

:::note
Acesta este doar un exemplu care să ilustreze conceptual ideea de functor pentru un programator. În realitate, formalizarea sistemelor de tipuri sub forma de categorii este mult mai complexă.
:::

Ce putem observa aici este că functorul nu conservă structura internă a unui tip de date - aceasta este irelevantă într-un context funcțional - ci conservă structura relațiilor între tipurile de date. De asemenea, pentru alte tipuri generice cu diferite proprietăți, trebuie incluse mai multe obiecte și morfisme în categoria destinație pentru functor. Categoria destinație poate include alte obiecte și morfisme la care nu se mapează din categoria sursă.

# Compunere

La fel ca morfismele și functorii pot fi compuși, dacă avem functorii $F : C \rightarrow D$ și $G : D \rightarrow E$ atunci va exista un functor $G \circ F : C \rightarrow E$ care aplicat pe $x \in Ob(c)$ și $f \in Hom(C)$ duc în $G(F(x)) \in Ob(E)$ respectiv $G(F(f)) \in Hom(E)$.

# Functorul identitate

Un caz special de (endo)functor este functorul identitate $Id_C : C \rightarrow C$ (endofunctorii sunt functori cu sursa și destinația aceiași categorie) care satisface proprietățile:

$$
Id_C(x) = x,\ \forall x \in Ob(C)
$$

$$
Id_C(f) = f,\ \forall f \in Hom(C)
$$

Astfel acest functor pentru conpunerea oricare functori $F : X \rightarrow C$, $G : C \rightarrow X$ satisface relațiile:

$$
Id_C \circ F = F
$$

$$
G \circ Id_C = G
$$

# Resurse

* [ncatlab](https://ncatlab.org/nlab/show/functor)