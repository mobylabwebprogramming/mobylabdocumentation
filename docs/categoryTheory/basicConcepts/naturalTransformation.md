---
title: Transformări naturale
sidebar_position: 3
---

În secțiunea anterioară am discutat despre functori dar mai important decăt funtori în teoria categoriilor sunt **transformările naturale**. Informal o transformare naturală este mapare între functori, print-o transformare naturală un functor este transformat în altul.

## Definiție: Transformare naturală

Dându-se două categorii $D$, $C$ și doi functori $F$, $G : C \rightarrow D$, o transformare naturală $alpha : F \implies G$ este  o colecție de morfisme care pentru $\forall x \in Ob(C)$ $\exists \alpha_{x} = F(x) \rightarrow G(x) \in Hom(D)$, numită componenta lui $\alpha$ în $x$, astfel încât pentru oricare morfism $f : x \rightarrow y \in Hom(C)$ următoarea diagramă **comută**:

<img alt="nat" src="/img/category-theory/natural-transformation.png" width="200" />

Când ne referim că acestă diagramă comută ne referim la faptul că perechile de morfisme se compun pe diagonală în același morfism, astfel trebuie să fie satisfacută următoarea relație:

$$
\alpha_y \circ F(f) = G(f) \circ \alpha_x
$$

# Compunere

Și în cazul transformărilor naturale acestea se pot compune ca morfismele și functorii. Având functorii  $F$, $G$, $H : C \rightarrow D$ și transformările naturale $\alpha : F \implies G$, $\beta : G \implies H$ atunci va exista mereu o transformare naturală $\beta \circ \alpha : F \implies H$ care are componentele $\forall x \in Ob(C)$ $\beta_x \circ \alpha_x \in Hom(D)$.

# Legătura cu programarea

În programare se folosesc des transformările naturale deși nu sunt știute sub acest nume. Cea mai uzuală transformare naturală folosită în programare este funcția **map/fmap/select** a tipurilor generice care impune o tranformare naturală de la functor identitate către aplicarea unui tip generic care implementează acesta functie. De exemplu, putem vedea cum se aplică acest lucru la clasa generică pentru liste în diagrama următoareȘ

<img alt="nat" src="/img/category-theory/natural-transformation-ex.png" width="400" />

Morfismele verticale care sunt compentele transformării naturale reprezintă aplicarea tipului generic peste tipurile initiale iar funcția **map** este cea care adaugă existența morfismelor orizontale astfel încât diagrama să comute.

# Resurse

* [ncatlab](https://ncatlab.org/nlab/show/natural+transformation)