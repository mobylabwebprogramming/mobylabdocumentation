---
title: Teoria categoriilor
sidebar_position: 1
---

## Contribuitor
[Silviu-George Pantelimon](https://teams.microsoft.com/l/chat/0/0?users=silviu.pantelimon@upb.ro)

# Disclaimer

Această secțiune este încă în lucru.

# Introducere

**Teoria categoriilor** este o teorie matematică fondată inițial de Saunders Mac Lane (4 august 1909 – 14 aprilie 2005) și Samuel Eilenberg (30 septembrie 1913 – 30 ianuarie 1998) și a reprezentat o contribuție majoră pentru topologia algebrică. Deși majoritatea programatorilor sau persoanelor din zona științei calculatoarelor nu au auzit de teoria categoriilor, aceasta a influențat știința calculatoarelor prin diferite concepte.

Un domeniu puternic influențat de teoria categoriilor este **paradigma funcțională**. Conceptul de **functor**, de exemplu, este preluat din teoria categoriilor și reprezintă o unealtă indispensabilă pentru programarea modernă. Cele mai triviale exemple de functori sunt listele și opționalele (tipul maybe), care în programarea în limbaje de nivel înalt sunt omniprezente. În multe cazuri, programatorii utilizează și alte structuri matematice, cum sunt **monadele**, chiar dacă nu știu că așa se numesc. Un exemplu netrivial este conceptul de task/promise din limbajele care suportă programarea asincronă.

Deși teoria categoriilor poate fi un corp de cunoștințe valoros pentru un programator, aceste materiale sunt mai mult dedicate promovării acestor cunoștințe datorită faptului că sunt interesante.

# Resurse utile

Pentru majoritatea materialelor descrise aici, sursa principală va fi [ncatab](https://ncatlab.org/nlab/show/HomePage). ncatlab este o enciclopedie matematică orientată pe teoria categoriilor, topologie și alte teorii matematice și o recomandăm pentru mai multe informații. Aici vom prelua doar ceea ce este necesar pentru subiectele discutate, într-o formă condensată.

De asemenea, recomandăm cursurile înregistrate ale [dr. Bartosz Milewski](https://www.youtube.com/@DrBartosz) și [blogul său](https://bartoszmilewski.com/). Acestea sunt probabil cel mai bun punct de plecare pentru cei interesați în teoria categoriilor.

<iframe width="560" height="315" src="https://www.youtube.com/embed/I8LbkfSSR58?si=jvUCbHf2pGGFfCAn" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerPolicy="strict-origin-when-cross-origin" allowFullScreen></iframe>

