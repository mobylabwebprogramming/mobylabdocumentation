---
title: HTML și CSS în contextul React
sidebar_position: 3
---

## HTML

**HTML (HyperText Markup Language)** este limbajul standard de markup folosit în aplicații web. Pentru a putea dezvolta aplicații web dezvoltatorii trebuie să cunoască HTML. Un fișier HTML este compus ca orice fișier **XML** dintr-un arbore de sintaxă sub formă de elemente HTML standard imbricate. Un element HTML este format:
* Dintr-un tag de început "\<nume_tag\>"
* Un tag de final "\</nume_tag\>"
* Atribute specificate în tagul de început \<nume_tag nume_atribut1="valoare_atribut1" nume_atribut2="valoare_atribut2"\>
* Elemente copil specificate între tagul de început și sfârșit, elemente fără copii se pot specifica \<nume_tag /\>

Un fișier HTML are următoarea structură generală:

```html showLineNumbers
<!DOCTYPE html> <!-- Declarația tipului documentului -->
<html> <!-- Declarația codului HTML -->
    <head> <!-- În elementul head se declară metadate, scripturi, stiluri pentru elemente și cateva elemente de afișat în browser -->
        <title>Page Title</title> <!-- Elementul title declară titlul paginii care să se afișeze în tabul din browser -->
    </head>
    <body> <!-- În body declarăm elementele care să se afișeze în pagina randată de browser -->
        <h1>My First Heading</h1> <!-- Putem declara un antet text cu taguri h1-6 -->
        <p>My first paragraph.</p> <!-- Putem declara paragrafe cu elementul p -->
        <a href="https://en.wikipedia.org/wiki/HTML">My first link</a> <!-- Aici avem un text cu link care ia ca atribut URL la care se face trimitere -->
    </body>
</html>
```

Deoarece HTML este un limbaj foarte vast vom face referire la [acest tutorial](https://www.w3schools.com/html/default.asp) pentru mai multe detalii. Cu toate acestea, pentru a lucra cu diverse biblioteci sau framework-uri web este necesară doar cunoașterea structurii generale a unui element HTML. În React, de exemplu, componentele sunt folosite exact ca elemente HTML cu atribute și elemente copil, dezvoltatorii pot să-și definească propriile componente sub acesta forma folosind componente predefinite care apoi sunt traduse în elemente HTML clasice. Astfel, dezvoltatorii doar trebuie să definească în modular componenta cu componenta aplicația. În plus, există diferite biblioteci precum Materia UI care expun componente deja customizate și stilizate.

Există totuși mici diferențe în modul lucru într-un fișier TSX din React față de declarația obișnuită de cod HTML:

```tsx showLineNumbers
import React, { PropsWithChildren } from 'react';
import ReactDOM from 'react-dom/client';

const MyLink = (props: PropsWithChildren<{ text: string, url: string }>) => {
  return <div>
    <a href={props.url}>{props.text}</a>
    {props.children}
  </div>
}

ReactDOM.render(<MyLink text='This is a link and a text below' url='https://google.com'>
              <p>Text as a child</p>
            </MyLink>, document.getElementById('root'));
```

Observați că valorile atributelor și a elementelor care sunt inserate în componente sunt declarate folosind **{}** cu variabilele declarate în funcție. De asemenea, atributele sunt pasate la componentă exact ca atributele iar copiii componentei sunt referențiați prin câmpul **children** dat de tipul **PropsWithChildren** care mai adaugă la tipul dat ca parametru de genericitate acest câmp.

## CSS

Folosind HTML putem declara cum o să arate vizual aplicația dar vrem să și customizăm aspectul aplicației web. Din acest motiv, pentru a depăsi limitările limbajului HTML a fost creat standardul **CSS (Cascading Style Sheets)** pentru a stiliza componente cum ar fi ce culoare să folosească, cum să se alinieze sau ce font pentru text să folosească. Structura de CSS este următoarea:

<img src="/img/web-programming/cssSelector.gif" />

Declarația este un dicționar cheie valoare unde se specifica ce proprietate este stilizată cu ce valoare. Declarația este precedata de un selector, acesta poate fi:
* selectorul universal * care se aplică pe toate elementele
* lista de elemente peste care se aplică stilul, ex. `p, div, h1`
* id-ul unui element unde `id` este dat ca atribut, ex. `#test` se aplică pe toate elementele cu atributul `id="test"` 
* clasa unui element unde `class` este dat ca atribut, ex. `.test` se aplică pe toate elementele cu atributul `class="test"`
* clasa unui tip de elemente, ex. `p.test` se aplică pe toate elementele `p` cu atributul `id="test"`
* putem să folosim și selectoare pentru descendentii unei componente cu `>`, ex. `div > .center` va aplica stilul pe descendenții unui element `div` care au clasa `center`

Există și alte tipuri de selectoare speciale dar nu vom intra în detalii aici.

```css showLineNumbers
p {
  text-align: center;
  color: red;
}

#para1 {
  text-align: center;
  color: red;
}

.center {
  text-align: center;
  color: red;
}

p.center {
  text-align: center;
  color: red;
}

div > .center {
  text-align: center;
  color: red;
}
```

Stilurile declarate pot fi și inserate **inline** în codul HTML dar se recomandă în general să fie incluse în fișiere CSS distincte.

```html
<p style="font-size: 100px;">Large font<p>
```

Bineînțeles, standardul CSS este destul de vast și recomandăm să parcurgeți următorul [tutorial](https://www.w3schools.com/css/default.asp) pentru mai multe detalii. Cel mai dificil lucru de a lucra cu CSS pentru un dezvoltator este să-și dea seama ce proprietăți trebuie să aplice asupra diferitelor elemente din aplicație, proprietățile fiind foarte diverse.

În React multe biblioteci vin cu componente deja stilizate dar se pot customiza și acestea. Pentru asta, se poate importa direct fisierul **.css** în componenta TSX pentru a aplica stilul ca de exemplu:

```tsx
import "./HomePage.css"
```
Ca diferență față de HTML, într-un fișier TSX trebuie menționat ca cuvantul **class** este rezervat în limbajul JavaScript/TypeScript și din acest motiv atributul pentru declararea clasei unui element este **className** în loc de **class**.

De asemenea, se pot declara **inline** stilurile dar cu mențiunea că acestea devin obiecte în fișierul TSX:

```tsx
<p style={{fontSize: "100px"}}>Large font<p>
```

## SASS si SCSS

Pentru stilizare mai avansată se poate folosi **[SASS (Syntactically Awesome Stylesheet)](https://www.w3schools.com/sass/default.php)**, acesta este un preprocesor de CSS care are anumite avantaje față de CSS normal cum ar fi posibilitatea de a declara variabile și a importa variabile. Cu ajutorul SASS se poate modulariza stilizarea aplicației mai ușor decât în cazul CSS simplu. Pentru a folosi SASS trebuie instalat pachetul de NPM:

```sh
npm install sass
```

Fișierele SASS cu extensia **.sass** pot fi importate exact la fel ca fișierele CSS în componenta unde sunt folosite. Sintaxa SASS mai veche arată în felul următor:

```sass
@import ./otherColors

$colorRed: #FF0000

p.center
  text-align: center
  color: $colorRed
```

Sau se pot declara mai aproape de sintaxa de CSS ca fișiere **SCSS (Sassy CSS)** cu extensia **.scss**:

```scss
@import "./otherColors";

$colorRed: #FF0000;

p.center {
  text-align: center;
  color: $colorRed;
}
```

## Tailwind

Exista alternative la CSS cu este [Tailwind](https://tailwindcss.com/docs/installation/using-postcss). Tailwind este un framework peste CSS care faciliteaza definirea stilurilor direct in HTML fara sa fie nevoie de scris fisiere separate de CSS. 

Tailwind are avantajul de a fi mai usor de inteles si mai usor de aplicat decat CSS explicit si faciliteaza implementarea unui UI responsive la redimensionare ecranului. Dezavantajul este trebuie aplicat in mod explicit pe fiecare componenta unde este nevoie. Pentru a folosi Tailwind doar trebuie aplicat pe atributul `className` (pentru React, pentru HTML in general este atributul `class`) numele claselor corespunzatoare stilurilor necesare.

Exemplul anterior poate fi implementat cu Tailwind in React astfel:

```tsx
<p className="text-center text-red-500">This is a paragraph<p>
```