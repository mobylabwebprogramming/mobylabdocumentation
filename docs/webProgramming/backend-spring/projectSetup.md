---
title: Pregătirea unui proiect
sidebar_position: 2
---

Pentru a vă ajuta în implementarea proiectului de la laborator, puteți utiliza scheletul proiectului Spring disponibil pe [GitLab](https://gitlab.com/mobylabwebprogramming/dotnetbackend). Cu toate acestea, pentru a începe, vom crea un proiect de backend în IDE-ul preferat, selectând un template pentru Spring Boot, după cum se arată mai jos.

## IntelliJ IDEA

<img alt="img" src="/img/web-programming/createNewProject.png" />
<img alt="img" src="/img/web-programming/selectSPringBootProject.png" />
<img alt="img" src="/img/web-programming/selectSpringVersionAndDependencies.png" />

În cadrul unei aplicații Spring Boot, clasa principală joacă un rol esențial în inițializarea și configurarea contextului aplicației. Această clasă este adnotată cu `@SpringBootApplication`, care este o compoziție a trei adnotări fundamentale:

- **`@Configuration`**: Indică faptul că clasa poate declara metode `@Bean` și poate fi utilizată de containerul Spring pentru generarea și gestionarea bean-urilor.
- **`@EnableAutoConfiguration`**: Activează mecanismul de auto-configurare al Spring Boot, care încercă să configureze automat bean-urile aplicației pe baza dependențelor prezente în claspath.
- **`@ComponentScan`**: Instruiește Spring să scaneze pachetul curent și subpachetele acestuia pentru a descoperi și înregistra automat componentele, serviciile și alte clase adnotate cu stereotipuri Spring.

Pe lângă adnotarea `@SpringBootApplication`, clasa principală conține și metoda `main`, care servește drept punct de intrare al aplicației. În această metodă, se apelează `SpringApplication.run()`, care lansează aplicația. Această metodă realizează următoarele:

1. Creează o instanță a `ApplicationContext`.
2. Înregistrează toate bean-urile și setările necesare.
3. Pornește serverul web încorporat (cum ar fi Tomcat sau Jetty) dacă este o aplicație web.

<img alt="img" src="/img/web-programming/projectStructureSpringBoot.png" />

Veți găsi și un fișier `application.yaml`, care conține configurarea aplicației. Acesta va fi utilizat pentru a seta variabile folosite de backend. Veti gasi un fisier `pom.xml` care contine configurarea, structura, dependintele si informatiile necesare pentru a genera proectul. În plus, veți găsi un controller foarte simplu care va răspunde la cererile HTTP de la clienți. Este important să rețineți că în dezvoltarea de aplicații web se folosește frecvent Dependency Injection. Deși acest concept va fi detaliat mai mult în cele ce urmează, puteți observa în cod că componentele sau clasele din proiect, cum ar fi WeatherForecastController, nu sunt instantiate în mod explicit. Instantierea se face implicit de către framework, calculând dependențele componentei și injectându-le în componentă la instanțiere prin constructor.

Rulați aplicația de backend, iar în browser va fi deschisă o pagină cu Swagger, sau cu denumirea sa alternativă, OpenAPI Specification. Swagger-ul este o interfață simplă pentru testarea cererilor HTTP fără a fi nevoie de un client HTTP. De asemenea, descrie întregul API HTTP al serverului, inclusiv rutele și tipurile de date schimbate cu clientul. Încercați să executați o cerere către backend direct din pagina Swagger-ului. Această facilitate este importantă deoarece ușurează testarea backend-ului și descrie modul în care API-ul poate fi folosit, permițând generarea automată a clienților HTTP pentru aplicațiile care consumă API-ul expus.

<img alt="img" src="/img/web-programming/swagger.png" />

Pentru a crea un backend, este important să acumulați cunoștințele necesare. Acestea vor fi expuse treptat în următoarele secțiuni.

## Sarcini de laborator

Instalați uneltele necesare pentru laborator și cereți ajutor dacă întâmpinați dificultăți.

Urmați codul din `WeatherForecastController` și încercați să-l modificați mergând pe intuiție și testând cu Swagger. Dacă aveți întrebări, adresați-le asistentului/asistentei. Mai multe informații vor fi furnizate în laboratoarele următoare.

Descărcați template-ul de proiect de pe [GitLab-ul grupului nostru](https://gitlab.com/mobylabwebprogramming/dotnetbackend) și încercați să parcurgeți codul, folosind explicațiile din comentarii. Dacă nu este necesar pentru acest laborator, dar puteți să-l folosiți ca punct de plecare pentru proiectul de la laborator.
