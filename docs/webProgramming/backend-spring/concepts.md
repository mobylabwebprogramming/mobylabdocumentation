---
title: Concepte de baza in dezvoltarea de aplicatii web
sidebar_position: 1
---

# Concepte de baza in dezvoltarea de aplicatii web

În cadrul acestui laborator, vom împărți aplicația web în două componente distincte: **frontend** și **backend**. Frontend-ul reprezintă partea vizibilă a aplicației pentru utilizator și rulează în browserul acestuia. Având frontend-ul ca o entitate independentă, acesta va fi dezvoltat sub forma unei aplicații **SPA (Single-Page Application)**. Spre deosebire de un **MPA (Multi-Page Application)**, o aplicație SPA constă într-o singură pagină care își modifică dinamic conținutul în funcție de acțiunile utilizatorului și datele furnizate de backend.

Backend-ul reprezintă un **server HTTP** care expune date și servicii prin intermediul unui API, putând să expună și conținut static și pagini web. Într-o aplicație MPA, fiecare pagină servită către browser este generată prin intermediul unui **șablon HTML** și populată direct cu date de către server, urmând modelul de programare **MVC (Model-View-Controller)**. Însă, în acest laborator, vom opta pentru un SPA pentru frontend, astfel încât serverul să expună doar date și servicii prin intermediul unui **API**. În scopul simplificării, API-ul va fi de tip **REST (REpresentational State Transfer)**, unde datele schimbate între server și client sunt în format JSON.

Aplicațiile backend pot fi, de asemenea, împărțite în două categorii: **monolitice** și **bazate pe microservicii**. Un backend monolitic constă într-un singur proces care include un server HTTP și cuprinde întreaga logică a aplicației. În schimb, microserviciile reprezintă mai multe procese care colaborează pentru a implementa logica aplicației, fiecare microserviciu având propria responsabilitate distinctă în sistem. Există avantaje și dezavantaje pentru ambele abordări; în acest laborator, vom utiliza o arhitectură de tip monolit pentru a facilita implementarea și înțelegerea conceptelor.

:::danger
Laboratorul vă va ghida în crearea unui backend cu o arhitectură de tip monolit, în care întreaga logica a backend-ului este integrată într-un singur proces. Aceleași principii și concepte de dezvoltare se aplică și în cazul unei arhitecturi bazate pe microservicii. Cu toate acestea, arhitectura bazată pe microservicii este mai avansată, necesitând o perioadă mai lungă pentru a fi înțeleasă în întregime, și nu este recomandată pentru proiecte la început, mai ales dacă echipa de dezvoltare este redusă.
:::