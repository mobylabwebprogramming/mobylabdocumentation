---
title: Particularitati C#
sidebar_position: 7
---

Vom discuta aici câteva particularități ale programării în C# față de alte framework-uri de web; informații mai extinse le puteți găsi în [acest curs](/docs/dotnet/csharp-workshop-presentation). O particularitate importantă în C# este că există metode de extensie. Acestea sunt artificii de compilator care adaugă metode noi la clase deja existente și pot fi apelate obiectual ca oricare alte metode non-statice ale acestor clase extinse. Metodele de extensie sunt folositoare pentru a extinde funcționalitatea pentru cod deja existent fără a interveni asupra acestuia.

Mai jos aveți exemple de metode de extensii pentru clasa WebApplicationBuilder; practic, la WebApplicationBuilder se adaugă metoda **.UseLogger()** pentru a adăuga un logger mai complex la acest builder. Cuvântul cheie **"this"** la primul parametru în metoda aceasta statică din clasa statică specifică pentru compilator să fie folosit ca și când primul parametru este "this", iar metoda este o metodă non-statică a acestuia.

```csharp showLineNumbers
public static class WebApplicationBuilderExtensions
{
    public static WebApplicationBuilder UseLogger(this WebApplicationBuilder builder)
    {
        builder.Host.UseSerilog((_, logger) =>
        {
            logger
                .MinimumLevel.Is(LogEventLevel.Information)
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("System", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.Hosting.Lifetime", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .Enrich.WithMachineName()
                .Enrich.WithProcessId()
                .Enrich.WithProcessName()
                .Enrich.WithThreadId()
                .WriteTo.Console();
        });

        return builder;
    }
}
```

Un caz particular pentru utilitatea metodelor de extensie sunt operațiile de **LINQ** pe colecții. În secțiunea pentru interacțiunea cu baza de date s-a văzut cum se pot înșirui mai multe expresii funcționale pentru a extrage informații din baza de date. Metodele respective sunt aceleași pentru toate implementările de context de baza de date; ele sunt metode de extensie pentru "this" IEnumerable. Acestea doar apelează metodele publice ale acestei interfețe; aceeași interfață este implementată de colecții clasice din biblioteca standard a limbajului. Adică aceleași metode pot fi folosite și pentru a face prelucrări peste liste, de exemplu, și avantajează programatorul prin a folosi un nivel de abstractizare ridicat pentru contexte diferite, cum ar fi colecțiile și bazele de date.

O altă particularitate față de limbaje cum e Java este că în C# pe lângă metode și câmpuri, clasele pot avea proprietăți care sunt câmpuri cu getter și setter. Este preferabil să fie folosite proprietăți în locul câmpurilor dacă acestea trebuie să fie publice, pentru că acestea pot abstractiza și valori care se calculează pe loc și fără să fie explicit apelată o metodă.

O ultimă particularitate care face ca C# să semene și cu limbaje precum Kotlin este suportul pentru programare asincronă. Dacă urmăriți metodele din codul pentru laborator, veți observa că majoritatea returnează un Task și au cuvântul cheie **"async"**. Aceste metode sunt preluate de firele de execuție a aplicației și executate asincron. Nu se știe când anume sunt executate; sunt executate când sunt planificate de framework pentru a optimiza execuția lor. Mai multe task-uri se pot executa pe același fir de execuție, iar într-o funcție **"async"** se poate face **"await"** pe alt task ca să se suspende execuția task-ului curent și să fie așteptată execuția task-ului la care se face await.