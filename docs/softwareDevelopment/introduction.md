---
title: Instrumente pentru dezvoltarea programelor
sidebar_position: 1
---

<img alt="img" src="/img/softwareDevelopment/idp.png" width="400" style={{margin: "auto", display: "block"}} />

## Despre

Cursul are ca scop însușirea cunoștințelor legate de folosirea instrumentelor pentru dezvoltarea programelor și formarea deprinderilor practice pentru analiza și alegerea soluțiilor potrivite pentru crearea de produse software de complexitate ridicată. Acesta urmărește, de asemenea, dobândirea unui fundament solid (teorie și aplicatii practice) privind instrumentele avansate de dezvoltare a programelor. Cursul include o introducere asupra mediilor, limbajelor, instrumentelor moderne de dezvoltare a programelor, prezintă caracteristicile unor medii de dezvoltare de ultimă generație, noțiuni privind programarea interfețelor cu utilizatorii, tehnologiile existente ce permit dezvoltarea programelor ce interfațează cu baze de date, componente și aplicații web. După absolvirea cursului, studentul va putea să proiecteze soluții corecte de aplicații folosind componente software existente și integrarea în diverse arhitecturi de dezvoltare, să dezvolte un sistem prin integrarea de diverse componente și să analizeze proprietățile sistemului, și să evalueze critic cele mai relevante tehnologii existente în contextul posibilei integrări a acestora în diverse sisteme proiectate. La laborator, se studiază Docker și Docker Swarm ca unelte de dezvoltare a aplicațiilor portabile bazate pe microservicii.

## Echipa

### Curs

- Cătălin Goșman

### Laborator

- Alexandra Ion
- Andreea Borbei
- Teia Vava
- Florin Mihalache
- Radu Ciobanu

## Orar

### Curs

* Miercuri 8-10

### Laborator

* Marți 14-16, Alexandra Ion
* Marți 16-18, Andreea Borbei
* Marți 18-20, Florin Mihalache
* Miercuri 12-14, Alexandra Ion
* Miercuri 14-16, Radu Ciobanu
* Joi 18-20, Teia Vava

## Calendar

<div style={{textAlign: "center"}}><iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQnd4o11JjYKNHwuaMW7pPn_rj8gbMq15gKffZRhiLybkm-YeWSckO9s_pRaRlIlD_jtid3aX9WTTH-/pubhtml?gid=100458238&amp;single=true&amp;widget=true&amp;headers=false"></iframe></div>