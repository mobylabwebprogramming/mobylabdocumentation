---
title: Terraform + Docker
sidebar_position: 4
---

În laboratorul curent o să folosim Terraform împreună cu Docker, cu care suntem deja familiari. Începem prin a crea un director nou pentru configuratia noastră:
```shell
mkdir lab4cc
cd lab4cc 
```

Creăm un fișier cu numele main.tf și introducem următorul cod:
```terraform
terraform {
 required_providers {
   docker = {
     source = "kreuzwerker/docker"
     version = "~> 2.13.0"
   }
 }
}
 
provider "docker" {}
 
resource "docker_image" "nginx" {
 name         = "nginx:latest"
 keep_locally = false
}
 
resource "docker_container" "nginx" {
 image = docker_image.nginx.latest
 name  = "tutorial"
 ports {
   internal = 80
   external = 8000
 }
}
```

Să analizăm pe rând fiecare bloc de cod introdus:

- `terraform {}` - conține setarile de Terraform, inclusiv provider-ul pe care urmează să îl folosim. Pentru fiecare provider, câmpul source definește numele provider-ului. By default, Terraform folosește Terraform Registry pentru a instala un provider. Astfel, în exemplul nostru, `kreuzwerker/docker` este un provider care se găsește în registrul Terraform la path-ul `registry.terraform.io/kreuzwerker/docker`. Câmpul `version` este opțional, dacă nu îl folosim, Terraform o să descarce by default ultima versiune disponibilă.
- `provider {}` - un block de tipul provider conține configurările necesare pentru ca acel provider să poată fi folosit. În cazul nostru, am folosit docker. Un provider este doar un plugin pe care Terraform îl folosește pentru a crea și pentru a gestiona resursele.
- Blocurile de tip `resource` - un astfel de bloc, așa cum sugerează și numele, este folosit pentru a defini resurse ale infrastructurii noastre. Așa cum putem observa, un bloc de tip resursă are 2 labels: `resource "docker_image" "nginx"`. În acest exemplu, `docker_image` este tipul de resursă, iar nginx este numele resursei. Fiecare astfel de bloc are mai multe proprietăți, acestea diferă de la resursă la resursă. Exemplu: în laboratorul viitor o sa configurăm o mașină virtuală într-un provider de cloud unde o să folosim parametrii ca tipul de masină, hard disk, dimensiune hard disk, regiunea în care să fie deployed masina, etc.

Recapitulare cod:
- Am populat block-ul `terraform` cu config-ul unde am specificat providerul docker și versiunea dorită.
- Am inițializat providerul `docker`.
- Am creat o resursă de tipul `docker_image` cu numele `nginx` (numele variabilei).
- Am creat o resursă de tipul `docker_container` cu numele nginx. În această resursă, pentru imagine am folosit imaginea definită mai sus, iar numele container-ului este dat de câmpul name.

Pentru a pune in picioare infrastructura:
- Initializare: `terraform init.` Urmăriți output-ul comenzii pentru a înțelege ce face această comandă.
- Formatarea codului. Acest pas este opțional, dar foarte util: `terraform fmt`
- Validarea configurării (și acest pas este optional, în cazul în care mergem mai departe cu o configurare invalidă, o să primim eroare): `terraform validate`
- Rulăm comanda `terraform plan`. Această comandă este un dry-run, deci putem observa cum o să arate infrastructura noastră după ce o să aplicăm configurarea creată anterior. **Atenție**, această comandă nu modifică infrastructura.
- Aplicăm configurația: `terraform apply`

:::note Task
Urmați instrucțiunile din shell-ul interactiv și faceți deploy la infrastructură. Observați output-ul comenzii.
Pentru a observa state-ul: `terraform show`
:::

:::note Task
Modificarea unei infrastructuri existente - modificati fișierul anterior, astfel încât container-ul să folosească portul 8080, nu 8000
Rulați comanda pentru a vedea planul. Modificarea este facută in-place?

Ștergere infrastructură - rulați comanda `terraform destroy`
:::