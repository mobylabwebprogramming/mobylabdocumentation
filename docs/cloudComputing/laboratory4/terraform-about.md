---
title: Ce este Terraform?
sidebar_position: 2
---

[Terraform](https://developer.hashicorp.com/terraform) reprezintă un instrument open-source folosit pentru Infrastructure as Code, dezvoltat de HashiCorp, prin care, mai exact, se automatizează procese de infrastructură / devops.

Un use-case principal al Terraform îl reprezintă conectarea la cloud providers, precum Google Cloud Platform (GCP) și Amazon Web Services (AWS), și gestionarea resurselor oferite de către cloud providers.

