---
title: Variabile în Terraform
sidebar_position: 5
---

Avem următorul fișier `outputs.tf`, unde avem acest bloc:
```terraform
output "Lab" {
  value="lab4 first output"
}
```
Am creat primul output în Terraform. Pentru a observa comportamentul, urmați pașii din exercițiul anterior pentru a "aplica" infrastructura.

Avem acest bloc cu următorul cod:
```terraform
variable "LabCCTerraform" {
  description = "primul lab in terraform"
  default = "valoare default a primului lab in terraform"
}
```
Aplicați planul și observați comportamentul.

## Prioritatea variabilelor
Un alt mod de a seta variabile în Terraform este prin variabile de mediu. By default, Terraform caută variabilele de mediu care încep cu `F_VAR_`. De exemplu, o variabilă de mediu cu numele `TF_VAR_MYTESTVAR=myvalue` o să atribuie valoarea myvalue unei variabile cu numele `MYTESTVAR`. Variabila cu numele `MYTESTVAR` trebuie sa fie totuși declarată in configurația noastră de Terraform.

:::note Task
- Creați o variabilă de mediu care sa suprascrie variabilă folosită de noi: `export TF_VAR_LabCCTerraform="Value from environment"`
- Rulați din nou comanda `terraform apply` pentru a observa comportamentul.
:::

## Fișiere tfvars
:::note Task
- Creați un nou fișier cu numele `terraform.tfvars `si adăugați următoarea linie de cod: `LabCCTerraform="Value from tfvars file"`. Acest fișier, deși are extensia diferită față de cele anterioare foloseste exact aceeași sintaxă.
- Folosiți din nou comanda `terraform apply` pentru a observa comportamentul.
:::

## Injectare variabile din linia de comandă
Un alt mod prin care putem atribui o valoare unei variabile este prin linia de comandă.

Rulați următoarea comandă: `terraform apply -var LabCCTerraform=valueFromCommandLine`

In urma acestui exercițiu, am aflat ca ordinea de prioritizare a variabilelor este următoarea:
1. linie de comandă
2. fișier tfvars
3. varibila de mediu
4. valoarea default

## Tipuri de variabile

Un alt tip de variabilă pe care îl putem folosi este lista:
```terraform
variable "mylist" {
  default = ["ana", "are", "mere"]
}
```

Pentru a accesa un element dintr-o listă avem 2 variante.
- folosind o funcție:
```terraform
output "getelement" {
  value = element(var.mylist,1)
}
```

- folosind accesare direct cu indexul:
```terraform
output "useindex" {
  value = var.mylist[1]
}
```

Liste nested:
```terraform
variable "nestedlist" {
  type = list
    default = [
      ["item1", "item2"],
      ["item3", "item4"]
    ]
}
```

Pentru a accesa elementele dintr-o listă nested, folosim sintaxa asemănătoare:
```terraform
output "nestedlist" {
  value = var.nestedlist[1][1]
}
```

Maps:
```terraform
variable mymap {
  default = {
    us = "United States"
    eu = "Europe"
    apac = "Asia/Pacific"
  }
}
output "my_region_map" {
  value = var.mymap
}
```

Pentru a accesa elementele dintr-un map, folosim functia lookup, mai multe detalii gasiți în link-ul: https://developer.hashicorp.com/terraform/language/functions/lookup
```terraform
output "oneMapElement" {
  value = lookup(var.mymap, "us")
}
```

## Organizarea codului Terraform

Având în vedere că deja devine greu de urmărit codul nostru, avem nevoie de o refactorizare.

Best practices Terraform recomandă să avem fișiere separate pentru variabile și outputs. Simplu spus, toate variabilele stau într-un fișier `variables.tf`, toate output-urile stau într-un fișier `outputs.tf`.