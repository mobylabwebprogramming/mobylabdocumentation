---
title: Cloud computing
sidebar_position: 1
---

<img alt="img" src="/img/cloud-computing/CC.png" width="400" style={{margin: "auto", display: "block"}} />

# Despre

Cursul introduce studenților noțiuni legate de Cloud Computing și îi pregătește pentru înțelegerea și folosirea corectă a conceptelor, modelelor și metodelor particulare de dezvoltare a Aplicațiilor și Serviciilor ce rulează în medii specifice de tip Cloud Computing. Cursul pornește de la prezentarea noțiunilor de bază specifice domeniului Cloud Computing, precum Infrastructure as a Service (IaaS), Container as a Service (CaaS), Platform as a Service (PaaS) și Software as a Service (SaaS), și ajunge să familiarizeze studenții cu elemente mai avansate de securitate sau modele economice de Business. La finalul cursului, studenții vor fi capabili să cunoască și să folosească corect modelele și mecanismele ce stau la baza mediilor și platformelor Cloud Computing. De asemenea, studenții vor căpăta deprinderile necesare pentru dezvoltarea corectă a Aplicațiilor și Serviciilor ce rulează în medii Cloud Computing, prin intermediul Docker și Kubernetes.

# Echipa

## Curs

- Master SSA: [Ciprian Dobre](mailto:ciprian.dobre@upb.ro)
- Master eGov: [Radu-Ioan Ciobanu](mailto:radu.ciobanu@upb.ro)

## Laborator

- [Andrei Damian](mailto:andreidamian0612@gmail.com)
- [Bogdan Georgescu](mailto:bgeorgescu@upb.ro)
- [Florin Mihalache](mailto:florin.razvan.mihalache@gmail.com)
- [Radu-Ioan Ciobanu](mailto:radu.ciobanu@upb.ro)

# Orar

| **Ora** 	|      **Luni**      	|     **Marti**    	|   **Miercuri**   	|      **Joi**      	|   **Vineri**   	|
|:-------:	|:------------------:	|:----------------:	|:----------------:	|:-----------------:	|:--------------:	|
|   8-10  	|  Florin (Lab SSA)  	|                  	|                  	| Andrei (Lab EGOV) 	|                	|
|  10-12  	| Ciprian (Curs SSA) 	|                  	|                  	|                   	|                	|
|  12-14  	|                    	| Bogdan (Lab SSA) 	|                  	| Bogdan (Lab EGOV) 	|                	|
|  14-16  	|                    	|                  	|                  	|                   	|                	|
|  16-18  	|                    	|  Radu (Lab EGOV) 	|                  	|                   	|                	|
|  18-20  	|                    	|                  	| Radu (Curs EGOV) 	|                   	|                	|