---
title: Instalare
sidebar_position: 2
---

Kubernetes se poate seta în multe moduri:

## Bootstrap la propriul cluster
Este metoda cea mai dificilă, în care se leagă mai multe mașini în rețea folosind [Kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/). Se folosește pentru a crea un cluster de la 0 și necesită un nivel de cunoștințe mediu-avansat.

## Folosirea unui cluster deja setat
Asa cum am precizat în introducere, sunt puține cazurile cand veți trebui să setați un cluster de la 0, deoarece vendorii principali de infrastructură au versiunile lor de Kubernetes și sistemele de CaaS deja pregătite.

## Instalare locală
Cea mai buna metodă de a învăța este să vă setați local un cluster cu un singur nod de Kubernetes. Acest lucru se poate face în mai multe moduri:
- Pentru utilizatorii Windows PRO care au și Docker instalat, Kubernetes vine preinstalat
- Pentru utilizatorii Windows Non-Pro se folosește [Minikube](https://minikube.sigs.k8s.io/docs/start/)
- Pentru utilizatorii Linux, se folosește [MicroK8s](https://microk8s.io/)
