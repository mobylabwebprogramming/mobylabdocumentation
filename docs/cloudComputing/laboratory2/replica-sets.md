---
title: ReplicaSets
sidebar_position: 9
---

Un ReplicaSet are rolul de a menține un număr stabil de replici ale unui pod. Acest obiect este definit prin anumite câmpuri, ca de exemplu un label selector care specifică modul în care pot fi controlate pod-urile, un număr de replici care indică numarul de pod-uri pe care le vrem up and running și un template al pod-urile pe care le orchestrează.

Exemplu de ReplicaSet:
```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
 name: nume
spec:
 replicas: 4 # numărul de replici ale pod-ului
 selector:
  matchLabels:
    app: containerlabel
  template:
    metadata:
      name: pod-template
      labels:
        app: containerlabel
    spec:
      containers:
      - name: container
        image: nginx
```

Pentru detalii legate de ReplicaSets putem folosi următoarele comenzi:
```shell
kubectl apply -f testapp-rs.yaml                           # creează un ReplicaSet dintr-un fisier
kubectl get replicasets                                    # afișează lista de ReplicaSet-uri
kubectl describe rs <nume-replicaset>                      # afișează detalii despre un ReplicaSet
kubectl delete rs <nume-replicaset>                        # sterge un ReplicaSet
```

:::note Task
Creați un ReplicaSet pe baza [acestui fișier](https://gitlab.com/mobylab-cc/laborator-4/-/blob/main/testapp-rs.yaml). Aplicați comenzile de mai sus.
:::

Un ReplicaSet poate fi scalat prin două moduri:
1. deschidem fișierul de configurare, modificăm numărul de replici și aplicăm comanda de apply asupra fișierului de configurare
2. folosind comanda de scalare: `kubectl scale replicasets <nume-replicaset> –replicas=4`

:::note Task
Task: scalați ReplicaSet-ul creat anterior la 4 noduri (folosind oricare din metode), apoi ștergeti ReplicaSet-ul.
:::