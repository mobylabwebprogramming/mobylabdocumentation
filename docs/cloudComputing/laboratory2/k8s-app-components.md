---
title: Componentele unei aplicații Kubernetes
sidebar_position: 4
---

Kubernetes folosește o ierarhie logică de componente pentru aplicații:
- **Pod** - unitatea fundamentală de lucru. Un pod conține întotdeauna containere.
- **Controllere** - creează, actualizează și menține starea de rulare a pod-urilor. Echivalentul unui serviciu din Docker Swarm.
:::tip Notă
Exemple de controllere: Deployment, ReplicaSet, StatefulSet, DaemonSet, Job, CronJob
:::
- **Service** - endpoint de conectare în rețea. Se atașeaza unui pod. Echivalentul configurației de rețea din Docker Swarm.
:::tip Notă
Tipurile de services sunt: NodePort, ClusterIP și LoadBalancer.
:::
- **Storage** - obiecte care se ocupă de persistarea datelor. PersistentVolume (PV) și PersistentVolumeClaim (PVC). Asemanător cu Docker mounts.
:::tip Notă
Datorită PV și PVC, în Kubernetes se poate realiza persistarea datelor chiar și între mai multe noduri. În cadrul Docker Swarm eram limitați la utilizarea unui NFS extern.
:::
- **Namespace** - grup de obiecte într-un cluster. Asemanător cu stack din Docker Swarm
- **Configurations** - obiecte de configurație. Exemple de obiecte de configurație: Secrets, ConfigMaps

MicroK8s reprezintă o versiune de Kubernetes folosită pe scară mică, pentru testarea aplicațiilor în medii offline (pe local, pe mașini virtuale). Pentru laboratoarele de Cloud Computing, acesta poate fi folosit (nu este obligatoriu, puteți să alegeți ce doriți). Instrucțiuni legate de setup-ul MicroK8s le aveți [aici](https://microk8s.io/docs/getting-started).

