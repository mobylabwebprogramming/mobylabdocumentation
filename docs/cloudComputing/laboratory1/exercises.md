---
title: Exerciții
sidebar_position: 12
---

## Comenzi de bază
1. Aduceți în cache-ul local imaginea ***busybox*** din registrul oficial Docker.
2. Rulați un container de ***busybox*** care să execute comanda ***uptime***.
3. Rulați un container interactiv de ***busybox***. Odată ce ați intrat în el, executați comanda `wget google.com`, apoi ieșiți.
4. Rulați un container interactiv detașat (daemon) de ***busybox***. Odată ce l-ați pornit, atașați-vă la el și dați comanda id, apoi ieșiți.
5. Ștergeți toate containerele și imaginile create la punctele precedente.

## Crearea unei imagini
:::tip
Pentru exercițiile următoare, veți porni de la [această arhivă](/files/cloud-computing/homework1.zip), care conține o aplicație simplă Node.Js.
:::

1. Pornind de la cele două fișiere din arhivă, scrieți un Dockerfile care va crea o imagine urmărind pașii de mai jos:
    1. se va porni de la cea mai recentă versiune a imaginii oficiale de Node.Js, adică ***node:14.13.0-stretch***
    2. se va copia fișierul ***package.json*** din arhivă în directorul curent (./); acest fișier are rolul de a specifica dependențele aplicației Node.Js (de exemplu, framework-ul Express.js)
    3. se va rula comanda ***npm install*** pentru a instala dependețele din fișierul de la pasul precedent
    4. se va copia sursa ***server.js*** în directorul de lucru ***/usr/src/app/***
    5. se va expune portul 8080
    6. în final, se va menționa comanda de rulare a aplicației; astfel, se va rula fișierul ***/usr/src/app/server.js*** cu binarul ***node***.
2. Folosiți Dockerfile-ul scris anterior pentru a crea o imagine numită ***node.jstest***.
3. Porniți un container care să ruleze imaginea ***node.jstest*** pe portul 12345 în modul detașat (daemon). Verificați că funcționează corect intrând pe http://127.0.0.1:12345.

## Lucrul cu rețele, volume și bind mounts
:::tip
Pentru exercițiile următoare, veți porni de la [această arhivă](/files/cloud-computing/homework2.zip), care conține o aplicație Node.Js care realizează un API de adăugare de cărți într-o bibliotecă peste o bază de date PostgreSQL. Exercițiile de mai jos vă trec prin pașii necesari pentru a rula un container pentru o bază de date PostgreSQL și containerul cu aplicația în aceeași rețea, având persistență la oprirea containerelor.
:::

1. Pe baza surselor și a fișierului Dockerfile din arhiva de mai sus, construiți o imagine cu numele (tag-ul) ***api-laborator-1-image***.
2. Creați o rețea bridge numită ***laborator1-db-network***.
3. Creați un volum numit ***laborator1-db-persistent-volume***.
4. Porniți în background un container pentru o bază de date cu următoarele caracteristici:
    1. se va atașa un bind mount care va face o mapare între fișierul ***init-db.sql*** de pe mașina locală (acesta va fi sursa la flag-ul de bind mount și se găsește în arhiva de laborator) și fișierul ***/docker-entrypoint-initdb.d/init-db.sql*** din containerul care se va rula (acesta va fi destinația)
    2. se va atașa volumul ***laborator1-db-persistent-volume*** creat anterior (sursa) la calea ***/var/lib/postgresql/data*** din containerul care se va rula (destinația)
    3. se va rula containerul în rețeaua ***laborator1-db-network*** creată anterior
    4. se vor specifica următoarele variabile de mediu (într-o comandă de ***docker run***, acest se lucru se face astfel: ***docker run -e NUME=valoare***):
        1. variabila ***POSTGRES_USER*** cu valoare ***admin***
        2. variabila ***POSTGRES_PASSWORD*** cu valoarea ***admin***
        3. variabila ***POSTGRES_DB*** cu valoarea ***books***
    5. containerul rulat se va numi ***laborator1-db***
    6. se va rula imaginea ***postgres*** din registrul oficial.
5. Porniți în background un container cu imaginea ***api-laborator-1-image*** creată anterior, cu următoarele caracteristici:
    1. se va rula containerul în rețeaua ***laborator1-db-network*** creată anterior
    2. se vor specifica următoarele variabile de mediu:
        1. variabila ***PGUSER*** cu valoare ***admin***
        2. variabila ***PGPASSWORD*** cu valoarea ***admin***
        3. variabila ***PGDATABASE*** cu valoarea ***books***
        4. variabila ***PGHOST*** cu valoarea ***laborator1-db***
        5. variabila ***PGPORT*** cu valoarea ***5432***
    3. containerul rulat se va numi ***laborator1-api***
    4. containerul va expune portul 80 și îl va mapa la portul 5555 de pe mașina locală.
6. Verificați că cele două containere rulează corect și au conectivitate:
folosind [Postman](https://www.postman.com/) sau orice altă aplicație similară, realizați cereri de GET și POST pe http://localhost:5555/api/books (pentru un tutorial de Postman, puteți intra [aici](https://learning.postman.com/docs/getting-started/sending-the-first-request/))
la cererile de POST, se așteaptă un body JSON cu formatul `{"title":"titlu","author":"autor"}`
cererile de GET vor returna o listă de cărți adăugate prin cereri de POST.
7. Verificați că volumul pe care l-ați adăugat păstrează persistența datelor:
    1. opriți și ștergeți cele două containere
    2. reporniți cele două containere cu aceleași comenzi ca anterior
    3. trimiteți o cerere de GET
    4. dacă ați configurat corect, veți primi o listă cu cărțile adăugate anterior.