---
title: Rularea unui container
sidebar_position: 6
---

Am văzut mai sus cum putem rula un Hello World într-un container simplu, însă putem rula imagini mult mai complexe. Putem să ne creăm propria imagine sau putem descărca o imagine dintr-un registru, cum ar fi [Docker Hub](https://hub.docker.com/). Acesta conține imagini publice, care variază de la sisteme de operare (Ubuntu, Alpine, Amazon Linux, etc.) la limbaje de programare (Java, Ruby, Perl, Python, etc.), servere Web (NGINX, Apache), etc.

Pentru acest laborator, vom rula Alpine Linux, care este o distribuție lightweight de Linux (dimensiunea sa este de 5 MB). Primul pas constă în descărcarea imaginii dintr-un registru Docker (în cazul nostru, Docker Hub):
```shell
$ docker image pull alpine
```

Pentru a vedea toate imaginile prezente pe sistemul nostru, putem rula următoarea comandă:
```shell
$ docker image ls
 
REPOSITORY      TAG         IMAGE ID        CREATED         SIZE
alpine          latest      961769676411    4 weeks ago     5.58MB
```

Se poate observa mai sus că imaginea pe care am descărcat-o are numele ***alpine*** și tag-ul ***latest***. Tag-ul unei imagini reprezintă o etichetă care desemnează în general versiunea imaginii, iar ***latest*** este un alias pentru versiunea cea mai recentă, pus automat atunci când nu specificăm explicit niciun tag.

Odată descărcată imaginea, o putem rula într-un container. Un mod de a face acest lucru este prin specificarea unei comenzi care să fie rulată în interiorul containerului (în cazul nostru, pe sistemul de operare Alpine Linux):
```shell
$ docker container run alpine ls -l
 
total 56
drwxr-xr-x    2 root     root     4096 Aug 20 10:30 bin
drwxr-xr-x    5 root     root      340 Sep 23 14:34 dev
drwxr-xr-x    1 root     root     4096 Sep 23 14:34 etc
drwxr-xr-x    2 root     root     4096 Aug 20 10:30 home
[...]
```

Astfel, în exemplul de mai sus, Docker găsește imaginea specificată, construiește un container din ea, îl pornește, apoi rulează comanda în interiorul său. Dacă dorim acces interactiv în interiorul containerului, putem folosi următoarea comandă:
```shell
$ docker container run -it alpine
```

Dacă dorim să vedem ce containere rulează la un moment de timp, putem folosi comanda ls. Dacă vrem să vedem lista cu toate containerele pe care le-am rulat, folosim și flag-ul ***-a***:
```shell
$ docker container ls -a
 
CONTAINER ID        IMAGE          COMMAND        CREATED             STATUS                         NAMES
562c38bb6559        alpine         "/bin/sh"      17 seconds ago      Exited (0) 2 seconds ago       musing_dhawan
d21dacca2576        alpine         "ls -l"        56 seconds ago      Exited (0) 55 seconds ago      interesting_raman
```

Pentru rularea unei imagini într-un container în background, putem folosi flag-ul ***-d***. La pornire, va fi afișat ID-ul containerului pornit, informație pe care o putem folosi pentru a ne atașa la container, pentru a îl opri, pentru a îl șterge, etc.:
```shell
$ docker container run -d -it alpine
 
50c4aeaa66d9abdb12b51dff0a7d3eced13fed3f688ceb0414db09da7f4565c4
```

```shell
$ docker container ls
 
CONTAINER ID      IMAGE        COMMAND        CREATED             STATUS            PORTS     NAMES
50c4aeaa66d9      alpine       "/bin/sh"      5 seconds ago       Up 3 seconds                condescending_feynman
```

```shell
$ docker attach 50c4aeaa66d9
 
/ # exit
```

```shell
$ docker stop 50c4aeaa66d9
 
50c4aeaa66d9
```

```shell
$ docker container ls
 
CONTAINER ID      IMAGE        COMMAND        CREATED             STATUS            PORTS     NAMES
```

```shell
$ docker rm 50c4aeaa66d9
 
50c4aeaa66d9
```

```shell
$ docker container ls -a
 
CONTAINER ID      IMAGE        COMMAND        CREATED             STATUS            PORTS     NAMES
```