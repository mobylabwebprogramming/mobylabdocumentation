---
title: Deployments
sidebar_position: 2
---

Despre deployments ați învățat în cadrul laboratorului trecut. În cadrul laboratorului curent veți învăța despre upgrade-ul unui deployment și despre rollback-ul unui deployment.

Pentru un fișier de deployment putem să avem următorul template:
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: <> #name 
spec:
  replicas: <x> #no of replicas
  selector:
    matchLabels:
      key: value
  template:
    metadata:
      labels:
        pod-key: pod-value
    spec:
      containers:
      - name: nginx
        image: <> #you can use the nginx image
```

Dorim, de exemplu, să facem următorul deploymentul, cu numele `cc-dep-01`, cu 4 replici și cu un label-ul `app: lab3cc`:
```yaml
kind: Deployment
metadata:
  name: cc-dep-01
spec:
  replicas: 10
  selector:
    matchLabels:
      app: lab3cc
  template:
    metadata:
      labels:
        app: lab3cc
    spec:
      containers:
      - name: nginx
        image: nginx
```
:::note Tasks
- Creați deployment-ul cum știm deja: `kubectl apply -f lab3dep.yaml`
- Afișați obiectele de tipul ReplicaSet și Deployment pe care le aveți momentan în cluster.
- Afișați pod-urile care sunt up and running.
:::

Ajungem la partea de upgrade a unui deployment. Un use-case relevant este unul des întâlnit în ziua de azi, presupunem că a apărut o nouă versiune a aplicației și vrem să folosim versiunea curentă.

În cazul deployment-ului nostru, vrem să folosim o nouă versiune a webserver-ului aplicației noastre, deci vrem să folosim o versiune mai nouă a nginx.

:::note Tasks
- Creați un nou fișier plecând de la fișierul creat anterior. Schimbați imaginea folosită în `nginx:1.21.1`.
- Aplicați modificările făcute: `kubectl apply -f mynewdep.yaml`
- Listați pod-urile din cluster. Ce se întâmplă?
:::

În primul rând, un aspect foarte important este că pod-urile sunt upgraded secvențial, nu toate odată, după ce un număr de noi pod-uri au fost create, urmează să se șteargă cele vechi, procedeul se repetă până când toate pod-urile au fost updated.

:::tip
Așa cum puteți observa, numele unui pod are următoarea structură: `numeDeployment-identificatorReplicaSet-identificatorPod`
:::

:::note Task
Listați toate obiectele de tip ReplicaSet din cluster: `kubectl get rs # am folosit o prescurtare`
:::

Din câte puteți observa, avem două ReplicaSets care corespund aceluiași deployment. Motivul este pentru că în momentul în care am făcut upgrade deployment-ului, a fost creat un nou ReplicaSet cu nouă imagine de nginx folosită. Obiectul ReplicaSet vechi este și el păstrat pentru că există mereu o șansă să vrem să ne întoarcem la versiunea anterioară a aplicației.
