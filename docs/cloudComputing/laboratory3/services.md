---
title: Servicii
sidebar_position: 4
---

Care sunt serviciile cunoscute în Kubernetes? Care este diferența între ele? Ne interesează în mod special diferența dintre ClusterIP și NodePort. Pentru a instanția o aplicație simplă care conține o bază de date și un API care face interogări, avem nevoie, asa cum intuiți, de 2 pod-uri. Pentru a porni un pod de Postgres, avem nevoie de a seta variabilele `POSTGRES_USER` și `POSTGRES_PASSWORD`, dar și numele bazei de date pe care urmează să o folosim - `POSTGRES_DB`. Avem mai multe moduri prin care putem face asta, cel mai simplu exemplu (și cel mai nesigur) este cu un ConfigMap.

Exemplu:
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
 name: db-config-map
data:
 POSTGRES_USER: "admin"
 POSTGRES_PASSWORD: "admin"
 POSTGRES_DB: "books"
```

Când o să creăm pod-ul / deployment-ul, o să folosim acest ConfigMap pentru a injecta variabilele de mediu. Pentru a avea datele persistente, avem nevoie, asa cum știm din labul trecut, de un PV și de un PVC:
- `PersistentVolume` (PV):
```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
 name: postgres-pv-volume
 labels:
  type: local
spec:
 storageClassName: manual
 capacity:
   storage: 1Gi
 accessModes:
   - ReadWriteOnce
 hostPath:
   path: "/mnt/data"
```

- `PersistentVolumeClaim` (PVC):
```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
 name: posgress-pvc
spec:
 storageClassName: manual
 accessModes:
   - ReadWriteOnce
 resources:
   requests:
     storage: 1Gi
```

Pentru a crea pod-ul de Postgres, avem următorul fișier:
```yaml
apiVersion: v1
kind: Pod
metadata:
 name: db-pod
 labels:
   app: postgres
spec:
 containers:
   - image: postgres:10.4
     name: postgres
     volumeMounts:
     - name: myvol
       mountPath: /etc/config
     ports:
       - containerPort: 5432
     envFrom:
       - configMapRef:
           name: db-config-map
 volumes:
   - name: myvol
     persistentVolumeClaim:
       claimName: posgress-pvc
```

Pentru ca podul nostru de API să comunice cu cel de baze de date, trebuie să expunem pod-ul de Postgres printr-un serviciu de tip ClusterIP:
```yaml
apiVersion: v1
kind: Service
metadata:
 name: postgres
 labels:
   app: postgres
spec:
 type: ClusterIP
 ports:
  - port: 5432
 selector:
  app: postgres
```

Pentru a porni podul de API, avem următorul fișier:
```yaml
apiVersion: v1
kind: Pod
metadata:
 name: apipod
 labels:
   app: api
spec:
 containers:
   - image: andreidamian/lab3cc:first
     name: apicontainer
     env:
      - name: PGUSER
        value: "admin"
      - name: PGPASSWORD
        value: "admin"
      - name: PGDATABASE
        value: "books"
      - name: PGHOST
        value: "postgres"
      - name: PGPORT
        value: "5432"
```

După ce și acest pod este up and running, trebuie să îl expunem printr-un serviciu de tip NodePort pentru a putea comunica cu el de pe mașina noastră. Facem asta prin următoarea comandă (se poate face și printr-un fișier YAML): `kubectl expose pod apipod –port 80 –type=NodePort`

Apoi verificați portul asignat de noul serviciu (31598):
<img alt="img" src="/img/cloud-computing/image5.png" width="75%" style={{margin: "auto", display: "block"}} />

Folosiți Postman pentru a accesa API-ul deployed:
<img alt="img" src="/img/cloud-computing/image2.png" width="75%" style={{margin: "auto", display: "block"}} />

Avem următoarea eroare:
<img alt="img" src="/img/cloud-computing/image1.png" width="75%" style={{margin: "auto", display: "block"}} />

Avem această eroare pentru că baza de date nu a fost configurată. Pentru a configura baza de date avem mai multe variante, cea mai simpla (dar și cea mai ineficientă) este următoarea:
- obtineți un shell pe podul de Postgres: `kubectl exec -it db-pod bash`
- ne conectam la baza de date books cu username și parola definite în ConfigMap-ul pentru acest pod (admin/admin): `psql -h localhost -U admin --password books`
<img alt="img" src="/img/cloud-computing/image4.png" width="75%" style={{margin: "auto", display: "block"}} />
- rulați următorul script pentru a inițializa baza de date:
```sql
CREATE TABLE IF NOT EXISTS books (
    id uuid PRIMARY KEY NOT NULL ,
    title VARCHAR NOT NULL,
    author VARCHAR NOT NULL
);
```

Incercați din nou comanda GET din Postman. Inserați ceva în baza de date:
<img alt="img" src="/img/cloud-computing/image3.png" width="75%" style={{margin: "auto", display: "block"}} />

Stergeți pod-ul de Postgres și incercați să faceți GET. Ar trebui să primiți următoarea eroare:
<img alt="img" src="/img/cloud-computing/image6.png" width="75%" style={{margin: "auto", display: "block"}} />

Reporniți pod-ul de baza de date și executați un GET:
<img alt="img" src="/img/cloud-computing/image7.png" width="75%" style={{margin: "auto", display: "block"}} />