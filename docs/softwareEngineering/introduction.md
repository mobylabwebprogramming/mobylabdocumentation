---
title: Inginerie Software 2
sidebar_position: 1
---

## Despre

Cursul **Inginerie Software 2** este destinat studenților de anul IV din cadrul domeniului de studii Informatică Industrială. Componenta inovativă a cursului se reflectă în modalitatea prin care sistemele de programe sunt proiectate și realizate pentru eficiența sistemelor industriale în special al roboților. Astfel, obiectivele principale al acestui curs sunt însușirea cunoștințelor fundamentale, a principiilor de bază și a metodelor de inginerie a sistemelor de programare care deservesc sistemele de bază, componentele de integrare, cât și utilizatorul. 

## Proiect IS2

Organizațiile moderne necesită sisteme eficiente pentru gestionarea și urmărirea cererilor interne și externe. Acest sistem va servi ca punct central pentru toate solicitările, asigurând transparență și eficiență în procesul de rezolvare. Obiectivul acestei teme este dezvoltarea unei aplicații web de ticketing care să permită crearea eficientă a unor tickete pentru problemele și cererile lor într-un mediu organizational cetralizat.

Enunțul temei poate fi accesat [aici](https://curs.upb.ro/2024/pluginfile.php/215950/mod_assign/introattachment/0/IS2___Proiect___2024_2025-3.pdf?forcedownload=1)

Stabilirea echipelor se face [aici](https://docs.google.com/spreadsheets/d/1mixZ_zxJChJdwTy_EV-Zy-JIZ94u2TsiJS6GlFy8XaU/edit?usp=sharing).

Deadline: **17.01.2024**

## Curs

* [Bogdan-Costel Mocanu](https://teams.microsoft.com/l/chat/0/0?users=bogdan_costel.mocanu@upb.ro)

## Echipa

* [Silviu-George Pantelimon](https://teams.microsoft.com/l/chat/0/0?users=silviu.pantelimon@upb.ro)
