---
title: Laborator 1 - mediul de dezvoltare
sidebar_position: 2
---

Scopul acestui laborator este de a pune bazele dezvoltarii software prezentand diferite unelte care sa ne ajute in acest scop. In cadrul laboratorului ne vom folosi de limbajul **C#** pentru a expune concepte din dezvoltarea software practicata de industrie. 

Inainte de a incepe cu dezvoltarea trebuie sa pregatim mediul de dezvoltare. Avem nevoie de:

- Un **sitem de versionare**, vom folosi [git](https://git-scm.com/) impreuna cu [Gitlab](https://gitlab.com/)/[Github](https://github.com/). Sistemele de verionare au mai multe avantaje in a le folosi, in primul rand folosind un sistem de versionare pentru a gestiona codul putem avea mai multe versiuni ale codului sursa a aceluiasi program care sa fie particularizat pentru diferite instante iar dezvoltatorul poate sa-si aleaga pe ce versiune sa lucreze. In al doilea rand, un sistem de versionare gazduit in cloud cum este Gitlab faciliteaza colaborarea intre membri echipei de dezvoltare deoarece codul si modificarile pe acestea sunt partajate. Iar in al treilea rand, toate modificarile codului sunt inregistrate intr-un istoric cu informatii despre cine a facut o modificare si cand.
- Un **IDE (mediu de dezvoltare integrat)** care in cazul nostru va fi **[Rider](https://www.jetbrains.com/rider/)** pentru **.NET**. Chiar daca pentru a crea o aplicatie este nevoie doar de un editor de text si un compilator care sa transforme codul sursa intr-un executabil, un IDE accelereaza dezvoltarea software pentru ca ofera facilitati de autocompletare, debugging si analiza statica si astfel asigura calitate mai ridicata a codului.
- Un limbaj de programare iar pentru acest laborator acesta este **C#**. Limbajul **C#** este un limbaj similar cu Java ca sintaxa ruland pe framework-ul **.NET** intr-un mediu virtualizat la fel ca pe **JVM**. O introducere in limbajul **C#** o puteti gasi [aici](/docs/dotnet/introduction/).
- Un manager de pachete, pentru **NET** acesta se numeste **[nuget](https://www.nuget.org/)**

## Sistemul de versionare Git

Pentru a folosi git puteti instala de pe [site](https://git-scm.com/) utilitarul sau sa il instalati pentru Linux si Mac cu comanda urmatoare:

```sh showLineNumbers
sudo apt-get install git # Linux
brew install git # Mac
```

Initializarea unui **repository** de git in care vom salva codul se poate face in doua moduri. Prima, din linie de comanda si apoi urcand codul pe un server de git:

```sh showLineNumbers
git init
git remote add origin https://gitlab.com/my_project_group/my_project # "origin" este numele repository-ului la distanta folosit in comenzi urmat de url-ul proiectului
```

Iar a doua, creind repository-ul pe server si apoi descarcandu-l local cu comanda de clonare:

```sh showLineNumbers
git clone https://gitlab.com/my_project_group/my_project
cd my_project
```

Atunci cand lucram cu un repository de git vom crea de obicei un **branch** legat de o sarcina de lucru si ne mutam pe acesta unde putem lucra si submite **commit-uri** care reprezinta modificarile incrementale peste versiunea precedenta modificarilor curente. Un **brach** reprezinta o bifurcare in istoricul codului permitand mai multor utilizatori sa lucreze la cod plecand de la un punct comun.
```sh showLineNumbers
git branch staging # "staging" este numele noului branch creat, trebuie sa existe cel putin un commit inainte
git checkout staging # ne mutam pe branch-ul "staing"
git checkout -b staging # se creaza direct branch-ul de "staging" si ne mutam pe branch-ul "staing"
git checkout -b staing origin/staging # ne mutam pe branch-ul "staing" care se gaseste pe server la distnta (remote) numit "origin"
```

Ca sa salvam modificarile sub forma de commit-uri trebuie sa adaugam fisierele modificate la commit iar apoi sa il efectuam.

```sh showLineNumbers
git add . # adaugam toate modificarile pentru urmatorul commit
git add my_file_or_folder # adaugam toate modificarile dintr-un fisier sau folder pentru urmatorul commit
git reset HEAD my_file_or_folder # scoatem de la commit modificarile efectuate intr-un fisier sau folder
git commit -m "This is a relevant message" # cream commit-ul pe branch-ul current
```

Odata comise modificarile acestea exista doar local, pentru a fi trimise catre server trebuie sa se execute operatia de **push**.

```sh showLineNumbers
git push # se trimit noile commit-uri de pe brach-ul curent pe serverul la distnta (remote) implicit
git push origin # se trimit noile commit-uri de pe brach-ul curent pe remote-ul "origin"
git push origin main # se trimit noile commit-uri de pe brach-ul "main" pe remote-ul "origin"
git push -all # se trimit toate branch-urile
```

Alti utilizatori pot descarca noile commit-uri si branch-uri fie clonand proiectul daca nu il au, fie prin operatii de **pull** sau **fetch**.

```sh showLineNumbers
git pull # se descarca branch-ul curent
git pull origin # se descarca branch-ul curent de pe remote-ul "origin"
git fetch # se descarca noile branch-uri si commit-uti de pe server
git fetch origin # se descarca noile branch-uri si commit-uti de pe remote-ul "origin"
```

Ultima operatie pe care trebuie sa o discutam este operatia de **merge**, daca la operatia de **branch** cream o bifurcatie in istoricul de git, la **merge** unificam doua branch-uri iar modificarile pe cod se combina.

```sh showLineNumbers
git merge staging # se uneste branch-ul "staging" ca sursa in destinatia branch-ul curent pe care ne aflam.
```

:::danger
Aveti grija cand faceti **merge**, e posibil ca pe branch-uri diferite sa se fi modificat aceleasi fisiere si atunci vor aparea conflicte, de obicei acestea se rezolva manual folosind un utilitar lorcal cum ar fi **Git GUI**. De aceea, cei care lucreaza pe acelasi cod ar fi bine sa lucreze pe sarcini care necesita modificari pe fisiere diferite de cod pentru a evita astfel de situatii
:::

:::tip
De obicei cand se lucreaza in productie exista mai multe branch-uri cu diferite roluri. O abordare buna este sa aveti doua branch-uri, unul de **productie** si unul de **dezvoltare**, cand dezvoltatorii incep sa lucreze la o sarcina de lucru acestia creeaza un nou branch din cel de **dezvoltare** si aplica modificarile pe cod acolo, Dupa terminarea sarcinii se executa un **merge request** pe serverul de git din interfata web unde colegii pot sa vada modificarile si veni cu observatii asupra codului, proces ce se numeste **review**, si se face **merge** in branch-ul de dezvoltare odata aprobat. Cand echipa decide lansarea unei noi versiuni pentru productie branch-ul de **dezvoltare** i se face merge in branch-ul de **productie** dupa ce codul a fost testat.
:::

Mai trebuie sa mentionam ca puteti avea in folderul proiectului si fisiere pe care nu doriti sa le partajati, cum ar fi fisiere binare rezultate in urma compilarii. Din acest moditiv sistemul git poate ignora asemenea fisiere sau foldere cu ajutorul unui fisier **.gitignore** in interiorul proiectului care pe fiecare linie are calea relativa la un fisier sau folder care trebuie omis la commit-uri. Puteti vedea ca aceste fisiere sunt ignorate si cu comanda de **status** care va mai e de ajutor si sa verificati ce alte informatii despre starea sistemului de versionare.

```sh showLineNumbers
git status
```

## Primul proiect de C#

Folosind git noi putem partaja proiecte software foarte usor, pentru a avea insa cod o sa ne folosim de un proiect in .NET pentru a si vedea cum putem sa-l gestionam.

Odata instalat IDE-ul si dependentele la framework se poate crea din interfata un nou proiect. Pentru acest laborator vom folosi o aplicatie de tip **console app** in **.NET 8**. Veti observa structura de baza a unui proiect C# care este destul de similara cu cea a unui program Java sau C/C++.

```csharp showLineNumbers
using System; // Folosind cuvantul cheie using se pot importa clase/simboluri din namespace-ul cerut.

namespace MyNewProject; // Se declara namespace-ul curent in care se afla clasele declarate in fisier.

public class Program // Trebuie sa existe neaparat o clasa pentru a avea fuctia principala.
{
    /* Se declara functia statica Main ca punct de intrare a programului, parametri care pot fi omisi si 
     * reprezinta argumentele in linie de comanda a programului (nu este continut numele executabilului ca in C)
     */
	public static void Main(string[] args) {
	    // ....
    }
}
```

Cu proiectul creat puteti incepe exercitiile de mai jos.

# Exercitii

1. Instalati-va [Rider](https://www.jetbrains.com/rider/) cu [contul acatedmic de Jetbrains](https://www.jetbrains.com/community/education/#students) cu mail-ul institutional si [installer-ul de Visual Studio](https://visualstudio.microsoft.com/downloads/).
2. Creati un nou proiect pe [Gitlab](https://gitlab.com/) sau [Github](https://github.com/) si clonati-l local.
3. Creati un proiect cu o aplicatie de consola in folderul clonat cu Rider.
4. Rulati proiectul care printeaza "Hello world!".
5. Instalati cu [nuget](https://www.nuget.org/) pachetul F23.StringSimilarity.
6. Afisati valoarea calculata pentru similitudinea intre sirurile de caractere "Hello world!" si "Hello class!"
7. Faceti un commit cu proiectul si apoi dati push pe un branch nou pe server.

# Resurse

* [Lista de comenzi](https://www.geeksforgeeks.org/git-cheat-sheet/)
* [Website de invatare comenzi git](https://learngitbranching.js.org/)