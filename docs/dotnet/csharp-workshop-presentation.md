---
title: C# Workshop
sidebar_position: 1
---

# Fundamentele OOP in C#

<img alt="C#" src="/img/dotnet/csharp.webp" width="200" style={{float: "right"}} />

**Descriere**: Scoala de vara are ca scop introducerea notiunilor de programare orientata pe obiect (OOP) cu exemplificare pe C#, precum si dezvoltarea de aplicatii folosind framework-uri de UI precum WPF sau MAUI.

**Public tinta**: studenti de anul I-II

**Format**: fizic, 10-12 sesiuni de ~2 ore la cate 2 zile distanta

**Numar maxim de participanti**: 20

**Perioada de desfasurare**: incepand cu 13 iulie

**Prezentări**: Silviu-George Pantelimon

# Concepte ce vor fi atinse in acest workshop

1. Introducere in arhitectura .Net si paradigme de programare in general
2. Tipuri de date: tipuri valoare si tipuri referinta, primitive si obiecte
3. Clase in detaliu, modificatori de acces, concepte de static vs non-static
4. Mostenire si polimorfism, clase abstracte si interfete
5. Genericitate
6. Colectii, enumeratii si alte clase utile
7. Mostenire vs. clase partiale vs. metode de extensie
8. Exceptii si error handling
9. Thread-uri si task-uri
10. Operatii de IO, lucrul cu stream-uri
11. Reflectie si injectare de dependente
12. Aplicatii in .NET

Pentru a vedea mai clar conceptele prezentate aici puteti accesa proiectul cu exemple pe Gitlab la aceasta adresa https://gitlab.com/mobylabwebprogramming/dotnetworkshop.