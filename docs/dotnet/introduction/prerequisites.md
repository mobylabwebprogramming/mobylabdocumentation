---
title: Mediu de dezvoltare si reguli de cod
sidebar_position: 3
---

Pentru a incepe sa programati in C# trebuie sa aveti un **IDE** si pachetele pentru **.NET 8** (dependentele necesare vor fi instalate automat):

1. Pentru Windows recomandam **[Visual Studio 2022 Community](https://visualstudio.microsoft.com/vs/)** sau puteti accesa prin **[MyUPB](https://my.upb.ro/)** varianta **[Professional sau Enterprise](https://azureforeducation.microsoft.com/devtools)** 
2. Pentru Linux/MacOSX sau daca vreti o alternativa puteti folosi platforma **[DotUltimate](https://www.jetbrains.com/dotnet/)** cu **[Rider](https://www.jetbrains.com/rider/)** ca IDE. Puteti cere licenta academica pentru toata suita de la **Jetbrains** cu emailul institutional. Este posibil sa fie nevoie sa instalati SDK-ul pentru **[.NET 8](https://dotnet.microsoft.com/en-us/download/visual-studio-sdks)** separat.
3. **Bonus**: puteti folosi **Visual Studio** cu **ReSharper** de la **Jetbrains** pentru o experienta mai buna cu IDE-ul, dar o sa aveti nevoie de cel putin 16GB de RAM.

## Reguli de cod

- Nu declarati variabile la inceputul functiei, declarati-le imediat inainte de folosire cu initializare, cu cat e mai mare distanta intre declararea variabilelor pana la folosirea acestora cu atat este mai probabil sa introduceti erori.
- Folositi camel-case sau pascal-case acolo unde este nevoie, nu folositi niciodata snake-case. Puteti consulta conventiile de nume pentru C# [aici](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/coding-style/identifier-names).
- Blocurile din interiorul if/for/while trebuie sa fie mereu intre acolade chiar daca e doar o singura linie de cod acolo, altfel vor aparea greseli daca gresiti identarea.
- Evitati sa folositi variabile statice/globale, doar constantele trebuie sa fie statice si globale, variabilele statice/globale pot genera multe probleme in special la acces concurent.
- Folositi un serviciu de versionare cum este **GitHub/GitLab/Bitbucket** si faceti commit-uri mai multe cu mesaje sugestive ca sa nu pierdeti munca voastra daca gresiti.
- Nu creati functii kilometrice, e mai usor de analizat codul apoi.
- Preferati sa generati mereu valori noi in loc sa transmiteti informatii prin efecte laterale, asa puteti evita erori mai usor pentru ca este bine definit fluxul programului.
- Nu creati voi funtii utilitare pentru lucruri simple, exista deja in biblioteca standard aceste functii cum ar fi "string.IsEmptyOrWithespace" ca sa testati daca un sir de caratere este null, gol sau plin de spatii goale.
- Preferati sa folositi biblioteci prin **[Nuget](https://www.nuget.org/)** in loc sa va creati voi de la 0 infrastructura de cod. Folositi biblioteci care sunt intretinute, cu multe descarcari si compatibile cu vesiunea voastra de framework. 
- Folositi debugger-ul din **IDE** in loc sa printati variabile, este mai usor de gasit erori din interfata unde puteti vedea variabilele, evalua expresii si misca prin stack frame-uri decat sa faceti acest lucru manual. De asemenea, invatati sa va folositi de un IDE la capacitate ca sa va usurati munca.
- Consultati documentatia oficiala pentru C# de la **[Microsoft](https://learn.microsoft.com/en-us/dotnet/csharp/)**, la fel si documentatia pentru bibliotecile descarcate de pe **Nuget**.

:::tip Sfat
Trebuie accentuat faptul ca e nevoie sa folositi un IDE. Nu doar ca un IDE va va usura munca si gestiunea unui proiect software dar prin sugestiile pe care le face va va invata cum sa urmariti codul si ce probleme pot aparea prin analiza statica de cod pe care o face. In plus, debugger-ul integrat si alte unelte va vor folosi pentru ca eficientiza dezvoltarea si depanarea de erori.
:::

## Structura minima a unui program

Puteti vedea aici structura minima a unui program C#. De mentionat e ca nu exista functii in fara claselor iar clasele sunt organizate in namespace-uri.

```csharp showLineNumbers
using System; // Folosind cuvantul cheie using se pot importa clase/simboluri din namespace-ul cerut.

namespace MyNewProject; // Se declara namespace-ul curent in care se afla clasele declarate in fisier.

public class Program // Trebuie sa existe neaparat o clasa pentru a avea fuctia principala.
{
    /* Se declara functia statica Main ca punct de intrare a programului, parametri care pot fi omisi si 
     * reprezinta argumentele in linie de comanda a programului (nu este continut numele executabilului ca in C)
     */
	public static void Main(string[] args) {
	    // ....
    }
}
```

Un **namespace** este o grupare logica a claselor/structurilor din program, functioneaza similar ca namespace-urile din C++ si sunt echivalente la pachetele (packages) din Java. Exista confuzie intre un **namespace** si **assembly**. Un assembly este codul compilat pentru a fi servit runtime-ului de .NET sub forma unei biblioteci lincata dinamic (DLL) sau a unui executabil (EXE). Un assembly poate contine mai multe namespace-uri iar un namespace poate aparea in mai multe assembly-uri. 

## Exercitii

1.	Instalati Visual Studio/Rider.
2.	Creati un proiect de console app in Visual Studio/Rider si incercati sa scrieti un program minimal.
3.	Creati un repository de git pe [Gitlab](https://gitlab.com/) si adaugati un commit cu proiectul vostru din Visual Studio/Rider (folositi facilitatile IDE-ului pentru git).
